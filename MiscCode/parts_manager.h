//
//  parts_manager.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 11/10/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef parts_manager_h
#define parts_manager_h

#include "hash.h"

#include <map>

/**
 * Parts Manager
 * Manages parts which are set into named slots
 */
class PartsManager
{
public:
	
	/**
	 * Part
	 */
	class Part
	{
	public:

		/**
		 * Constructor
		 * @param slot	Slot name
		 */
		Part(hash_t slot)
		: m_slot(slot)
		{}
		
		/**
		 * Constructor
		 * @param slot	Slot name
		 */
		Part(const char* slot)
		: Part(hash(slot))
		{}
		
		/**
		 * Get slot name
		 */
		hash_t slot() const { return m_slot; }
		
		/**
		 * Is part compatible with this part
		 * @param part	Other part
		 * @return		True if the parts are compatible, false otherwise
		 */
		virtual bool isCompatible(const Part* part) { return true; }
		
	private:
		
		// Slot name
		hash_t m_slot;
	};
	
	// Slot Map
	typedef std::map<hash_t, Part*> SlotMap;
	
	/**
	 * Constructor
	 */
	PartsManager();
	
	/**
	 * Deconstructor
	 */
	~PartsManager();
	
	/**
	 * Get slot map
	 */
	const SlotMap& slots() { return m_slots; }

	/**
	 * Add a slot to be managed
	 * @param name	Slot name
	 * @param		True on success, false otherwise
	 */
	bool addSlot(hash_t name);
	
	/**
	 * Add a slot to be managed
	 * @param name	Slot name
	 * @param		True on success, false otherwise
	 */
	bool addSlot(const char* name);
	
	/**
	 * Set a part
	 * If oldPart is not set and if the slot is already occupied,
	 * the part will be deleted
	 * @param part		Part to set
	 * @param oldPart	Current part, if slot is occupied
	 * @return			True on success, false otherwise
	 */
	bool setSlot(Part* part, Part** oldPart = nullptr);
	
	/**
	 * Unset any part in the slot
	 * If oldPart is not set, the part occupying the slot will
	 * be deleted
	 * @param name		Slot name
	 * @param oldPart	Current part
	 * @return			True on success, false otherwise
	 */
	bool unsetSlot(hash_t name, Part** oldPart = nullptr);

	/**
	 * Unset any part in the slot
	 * If oldPart is not set, the part occupying the slot will
	 * be deleted
	 * @param name		Slot name
	 * @param oldPart	Current part
	 * @return			True on success, false otherwise
	 */
	bool unsetSlot(const char* name, Part** oldPart = nullptr);

	/**
	 * Get part occupying the slot
	 * @param name	Slot name
	 * @return		Part or null
	 */
	const Part* getSlot(hash_t name) const;

	/**
	 * Get part occupying the slot
	 * @param name	Slot name
	 * @return		Part or null
	 */
	const Part* getSlot(const char* name) const;
	
private:
	// Slot map
	SlotMap m_slots;
};


#endif
