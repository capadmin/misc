//
//  itoa.cpp
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/6/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#include <stdlib.h>

/**
 * Converts an integer value to a string using the specificied base
 * @param   value   Integer value
 * @param   buffer  String buffer
 * @param   len     Length of string buffer
 * @param   base    Numeric base to use
 * @return  String representation of the integer value, or a zero-length string on failure
 */
char* itoa(int value, char* buffer, size_t len, int base)
{
    // No buffer
    if( buffer == 0 || len == 0 )
        return buffer;
    
    // Invalid base
    if( base < 2 || base > 16 )
    {
        buffer = '\0';
        return buffer;
    }
    
    const char* DIGITS = "FEDCBA9876543210123456789ABCDEF";
    char* str = buffer;
    int temp;
    
    do
    {
        // For each iteration, find the remainder and the associated digit, and append
        temp = value;
        value /= base;
        *str = DIGITS[15 - (temp - value * base)];
        ++str;
        
        // Length check
        if( --len == 0 )
        {
            *buffer = '\0';
            return buffer;
        }
    }
    while( value );
    
    // Append '-', if negative
    if( temp < 0 )
    {
        *str = '-';
        ++str;
        
        if( --len == 0 )
        {
            *buffer = '\0';
            return buffer;
        }
    }
    
    // Append terminated null
    *str = '\0';
    --str;
    
    // Reverse string
    char* str1 = buffer;
    char c;
    while( str > str1 )
    {
        c = *str;
        *str = *str1;
        *str1 = c;
        --str;
        ++str1;
    }
    
    return buffer;
}