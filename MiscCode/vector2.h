//
//  vector2.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/28/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef vector2_h
#define vector2_h

#include <cmath>

class vector2
{
public:
    union
    {
        struct
        {
            float x;
            float y;
        };
        
        float a[2];
    };
    
    vector2(float a = 0.0f, float b = 0.0f)
    : x(a), y(b)
    {}
    
	void set(float a, float b) { x = a; y = b; }

    float lengthSqr() const
	{
		return (x * x) + (y * y);
	}
    
    float length() const
	{
		return sqrtf(lengthSqr());
	}
    
    void normalize()
    {
        float length = lengthSqr();
        if( length != 1.0f && length != 0.0f )
        {
            length = 1.0f / sqrtf(length);
            x *= length;
            y *= length;
        }
    }

	vector2 operator+(const vector2& right) const
	{
		return vector2(x + right.x, y + right.y);
	}

	vector2 operator-(const vector2& right) const
	{
		return vector2(x - right.x, y - right.y);
	}

	vector2 operator*(float s) const
	{
		return vector2(s * x, s * y);
	}

	void operator+=(const vector2& right)
	{
		x += right.x;
		y += right.y;
	}

	void operator-=(const vector2& right)
	{
		x -= right.x;
		y -= right.y;
	}

	void operator/=(float s)
	{
		x /= s;
		y /= s;
	}

	void operator*=(float s)
	{
		x *= s;
		y *= s;
	}

	float dot(const vector2& right) const
	{
		return (x*right.x) + (y*right.y);
	}

	float angleBetween(const vector2& right) const
	{
		float rightAngle = atan2f(right.y, right.x);
		if( rightAngle < 0.0f )
			rightAngle += (2 * M_PI);
		
		float thisAngle = atan2f(y, x);
		if( thisAngle < 0.0f )
			thisAngle += (2 * M_PI);
		
		float angle = rightAngle - thisAngle;
		
		if( angle < -M_PI )
			angle = (2 * M_PI) + angle;
		else if( angle > M_PI )
			angle = -(2 * M_PI) + angle;
		
		return angle;
	}

	void rotate(float angle)
	{
		const float c = cos(angle);
		const float s = sin(angle);

		const float a = x * c - y * s;
		y = x * s + y * c;
		x = a;
	}
};

/*
vector2 operator*(float s, const vector2& right)
{
	return right * s;
}
*/
#endif
