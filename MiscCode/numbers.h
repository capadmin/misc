//
//  numbers.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/24/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef numbers_h
#define numbers_h

#define _USE_MATH_DEFINES
#include <cmath>

#if !defined(MIN)
#define MIN(a, b)	((a) < (b) ? (a) : (b))
#endif

#if !defined(MAX)
#define MAX(a, b)	((a) > (b) ? (a) : (b))
#endif

#if !defined(CLAMP)
#define CLAMP(v, min, max)	((v) < (min) ? (min) : ((v) > (max) ? (max) : (v)))
#endif

#if !defined(DEG2RAD)
#define DEG2RAD(x)	((x) * (3.14159f / 180.0f))
#endif

#if !defined(RAD2DEG)
#define RAD2DEG(x)	((x) * (180.0f / 3.14159f))
#endif

inline bool isZero(float v) { return v > -0.001f && v < 0.001f; }

inline bool isEqual(float a, float b) { return isZero(a - b); }

inline float sign(float v) { return v >= 0.0f ? 1.0f : -1.0f; }

/**
 * Simple linear interpolation between a start and end value
 * @param   start   Start value
 * @param   end     End value
 * @param   t       Normalized distance from start, [0, 1]
 * @return          Interpolated value between start and end
 */
inline float linear(float start, float end, float t)
{
    // Determine the length traveled between start and end
    // represented by the t-value.
    // Add to start to determine the new value.
    return start + ((start - end) * t);
}
   
inline float linear(float speed, float value, float max, float delta)
{
    // Apply delta value to value
    // return the lowest value between value and max
    value += speed * delta;
    
    return (value < max ? value : max);
}

/**
 * Sine-based interpolation
 * Eases towards the maximum value quickly, and coasts to it
 * @param   value   Current value
 * @param   end     End value
 * @param   delta   Distance moved towards the end value [0, 1]
 * @return          Interpolated value
 */
inline float sine(float value, float end, float delta)
{
    // asinf takes the y-value ([0, 1]) as the parameter and returns
    // the x-value ([0, PI/2])
    // Add the delta value ([0, 1]) in the space of the x-value by
    // multiplying it by PI/2.
    // sinf takes the modify x-value, returns the new y-value
    // and that is multiplied by the end value to generate the
    // new value.
    return sinf(asinf(value / end) + (delta * M_PI_2)) * end;
}

/**
 * Power-based interpolation
 * Eases in, and then accelerates towards the maximum value very quickly
 * @param   value   Current value
 * @param   end     End value
 * @param   delta   Distance moved towards the end value [0, 1]
 * @return          Interpolated value
 */
inline float power(float value, float end, float delta)
{
    // sqrtf takes the y-value ([0, 1]) and returns the x-value ([0, 1])
    // Add the delta value ([0, 1]) to the x-value, square it, and
    // multiple by the end value to generate the new value.
    float x = sqrtf(value / end) + delta;
    return (x * x) * end;
}

#endif