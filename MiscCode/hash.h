//
//  hash.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 11/9/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef hash_h
#define hash_h

#include <cstdint>

typedef uint32_t hash_t;

/**
 * FNV-1a Hash
 * http://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function
 */

// 32-bit FNV starter value
#define FNV_32_INIT ((uint32_t)0x811c9dc5)

/**
 * Perform a 32-bit Fowler/Noll/Vo FNV-1a hash on a string.
 * @param str	String to hash
 * @param hval	Hash seed value - 0 for consistency, FNV_32_INIT for lowest collisions
 * @return		Hash value
 */
hash_t hash(const char *str, hash_t hval = 0);

/**
 * Get the string value hashed the hash value
 * If DISABLE_UNHASH is not set, the hash's string value is returned.
 * If it is set, a string containing the hex value is returned.
 * @param hval	Hash value
 * @return		If found, string value, or nullptr
 */
const char* unhash(hash_t hval);

#endif
