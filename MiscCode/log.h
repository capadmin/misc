//
//  log.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/23/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef log_h
#define log_h

#if !defined(DISABLE_LOG)

/** Log an Error */
#define ERRORF(s, ...)	Log::logf(Log::eERROR, __FILE__, __LINE__, s, ## __VA_ARGS__)

/** Log a Warning */
#define WARNF(s, ...)	Log::logf(Log::eWARNING, __FILE__, __LINE__, s, ## __VA_ARGS__)

/** Log info */
#define INFOF(s, ...)	Log::logf(Log::eINFO, __FILE__, __LINE__, s, ## __VA_ARGS__)

/** Log unfiltered info */
#define LOGF(s, ...)	Log::logf(Log::eDEBUG, __FILE__, __LINE__, s, ## __VA_ARGS__)

class Log
{
public:
    
    // Log Levels
    enum Level
    {
        eDEBUG = 0,
        eERROR = 1,
        eWARNING = 2,
        eINFO = 3,
    };
    
    // Any log with a higher level than set is ignored
	static Level LevelFilter;
    
	/**
     *	Log a formatted message
     *	@param	level	Log level - A lower value represents a more significant message
     *	@param	file	Originating file
     *	@param	line	Originating line
     *	@param	msg		Message
     *	@param	...		Message args
     */
	static void logf(Level level, const char* file, int line, const char* msg, ...);
};

#else

#define ERRORF(s, ...)
#define WARNF(s, ...)
#define INFOF(s, ...)
#define LOGF(s, ...)

#endif

#endif
