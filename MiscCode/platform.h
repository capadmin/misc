#if defined(__APPLE__)

#elif defined(WIN32)

#define	snprintf	sprintf_s
#define vsnprintf(dst, len, format, ...)	vsnprintf_s(dst, len, len - 1, format, ## __VA_ARGS__);

#endif