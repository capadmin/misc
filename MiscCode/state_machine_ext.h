//
//  state_machine_ext.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 11/8/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef state_machine_ext_h
#define state_machine_ext_h

#include "state_machine.h"

//// Numeric Event/Transition

class Number
{
public:
	enum Type
	{
		INTEGER,
		FLOAT
	};

	Number(int v)
	: m_iValue(v)
	, m_type(INTEGER)
	{}
	
	Number(float v)
	: m_fValue(v)
	, m_type(FLOAT)
	{}
	
	Type type() const { return m_type; }
	bool isFloat() const { return m_type == FLOAT; }
	bool isInteger() const { return m_type == INTEGER; }
	
	int32_t iValue() const { return m_type == INTEGER ? m_iValue : (int)m_fValue; }
	float fValue() const { return m_type == FLOAT ? m_fValue : (float)m_iValue; }
	
	//int32_t operator int32_t() const { return iValue(); }
	//float operator float() const { return fValue(); }
	
	bool operator==(const Number& right) const
	{
		if( m_type == INTEGER && right.m_type == INTEGER )
			return m_iValue == right.m_iValue;
		else if( m_type == FLOAT && right.m_type == FLOAT )
			return m_fValue == right.m_fValue;
		else if( m_type == INTEGER )
			return m_iValue == right.m_fValue;
		else
			return m_fValue == right.m_iValue;
	}
	
	bool operator!=(const Number& right) const
	{
		return !(*this == right);
	}
	
	bool operator<(const Number& right) const
	{
		if( m_type == INTEGER && right.m_type == INTEGER )
			return m_iValue < right.m_iValue;
		else if( m_type == FLOAT && right.m_type == FLOAT )
			return m_fValue < right.m_fValue;
		else if( m_type == INTEGER )
			return m_iValue < right.m_fValue;
		else
			return m_fValue < right.m_iValue;
	}
	
	bool operator>(const Number& right) const
	{
		return (right < *this);
	}
	
	bool operator>=(const Number& right) const
	{
		return (*this > right || *this == right);
	}
	
	bool operator<=(const Number& right) const
	{
		return (*this < right || *this == right);
	}
	
private:
	union
	{
		int32_t m_iValue;
		float m_fValue;
	};
	
	Type m_type:1;
};

class NumericEvent : public StateMachine::Event
{
public:
	static const uint16_t Id = 1;
	
	NumericEvent(hash_t name, int32_t value)
	: StateMachine::Event(Id, name)
	, m_value(value)
	{ }
	
	NumericEvent(const char* name, int32_t value)
	: NumericEvent(hash(name), value)
	{}
	
	NumericEvent(hash_t name, float value)
	: StateMachine::Event(Id, name)
	, m_value(value)
	{ }
	
	NumericEvent(const char* name, float value)
	: NumericEvent(hash(name), value)
	{}
	
	const Number& value() const { return m_value; }
	
protected:
	Number m_value;
};

class NumericCompareTransition : public StateMachine::Transition
{
public:
	enum Comparison
	{
		eEQUAL,
		eNOT_EQUAL,
		eLESS_THAN,
		eLESS_THAN_EQUAL,
		eGREATER_THAN,
		eGREATER_THAN_EQUAL,
	};
	
	NumericCompareTransition(hash_t eventName, hash_t fromState, hash_t toState, Comparison compare, int32_t baseValue)
	: StateMachine::Transition(NumericEvent::Id, eventName, fromState, toState)
	, m_baseValue(baseValue)
	, m_comparison(compare)
	{}
	
	NumericCompareTransition(const char* eventName, const char* fromState, const char* toState, Comparison compare, int32_t baseValue)
	: NumericCompareTransition(hash(eventName), hash(fromState), hash(toState), compare, baseValue)
	{}
	
	NumericCompareTransition(hash_t eventName, hash_t fromState, hash_t toState, Comparison compare, float baseValue)
	: StateMachine::Transition(NumericEvent::Id, eventName, fromState, toState)
	, m_baseValue(baseValue)
	, m_comparison(compare)
	{}
	
	NumericCompareTransition(const char* eventName, const char* fromState, const char* toState, Comparison compare, float baseValue)
	: NumericCompareTransition(hash(eventName), hash(fromState), hash(toState), compare, baseValue)
	{}
	
	virtual bool activates(const StateMachine::Event& event) const
	{
		if( !StateMachine::Transition::activates(event) )
			return false;

		const NumericEvent& numericEvent = (const NumericEvent&)event;
		
		switch( m_comparison )
		{
			case eEQUAL:				return numericEvent.value() == m_baseValue;
			case eNOT_EQUAL:			return numericEvent.value() != m_baseValue;
			case eLESS_THAN:			return numericEvent.value() < m_baseValue;
			case eLESS_THAN_EQUAL:		return numericEvent.value() <= m_baseValue;
			case eGREATER_THAN:			return numericEvent.value() > m_baseValue;
			case eGREATER_THAN_EQUAL:	return numericEvent.value() >= m_baseValue;
		}
		
		return false;
	}
	
protected:
	Number m_baseValue;
	
	Comparison m_comparison:3;
};

#endif
