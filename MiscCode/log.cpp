//
//  log.cpp
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/23/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#if !defined(DISABLE_LOG)

#include "log.h"

#include <stdarg.h>
#include <string.h>
#include <stdio.h>

#if defined(WIN32)
#include <windows.h>
#endif

#include "platform.h"

Log::Level Log::LevelFilter = Log::eINFO;

void Log::logf(Log::Level level, const char* file, int line, const char* msg, ...)
{
    if( level > LevelFilter )
        return;
    
	const size_t LENGTH = 4096;
	char log[LENGTH];
    
    const char* levelName = NULL;
    switch(level)
    {
        case eERROR:    levelName = "Error"; break;
        case eWARNING:  levelName = "Warning"; break;
        case eINFO:     levelName = "Info"; break;
        case eDEBUG:    levelName = "Debug"; break;
    }
    
#if defined(WIN32)
	const char* sf = strrchr(file, '\\');
#else
	const char* sf = strrchr(file, '/');
#endif
    
	if( sf != NULL )
		file = sf + 1;
    
    int headerLength = snprintf(log, LENGTH, "%s :: %s:%d - ", levelName, file, line);
    if( headerLength == -1 )
        headerLength = 0;
    
    va_list args;
	va_start(args, msg);
    
	int lineLength = vsnprintf(log + headerLength, LENGTH - headerLength, msg, args);
    
	va_end(args);
    
    if( lineLength == -1 )
        return;
    
    int totalLength = headerLength + lineLength;
    if( totalLength > LENGTH )
        totalLength = LENGTH;
    
    if( log[totalLength-1] != '\n' )
    {
        if( totalLength + 2 >= LENGTH )
            totalLength = LENGTH - 2;
        
        log[totalLength] = '\n';
        log[totalLength+1] = '\0';
    }
    
#if defined(WIN32)
    OutputDebugStringA(log);
#else
    printf(log);
#endif
}

#endif