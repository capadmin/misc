//
//  memory_pool.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/24/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef memory_pool_h
#define memory_pool_h

/**
 * General memory manager
 * Contains a collection of memory pools.
 * It attempts to find the smallest block pool first,
 * and then attempts to allocate from the dynamic pools.
 */
class Memory
{
public:
    
    /**
     * A memory pool of uniformly sized blocks of memory.
     * Very fast alloc/dealloc, low overhead, and no fragmentation.
     * The the num of blocks must be a power of two great than 32.
     * The size of the blocks is unrestricted.
     */
	class BlockPool
	{
	public:
        /**
         * Constructor
         * @param size          Size of the memory blocks
         * @param num           Number of blocks - must be a power of two, >= 32
         * @param clearOnFree   Clear memory when freed
         */
		BlockPool(unsigned int size, unsigned int num, bool clearOnFree = false);
        
        /**
         * Deconstructor
         */
		~BlockPool();
    
        /**
         * Get the block size
         */
		unsigned int blockSize();
        
        /**
         * Get the number of blocks
         */
        unsigned int numBlocks();
        
        /**
         * Get the number of free blocks
         */
        unsigned int freeBlocks();

        /**
         * Alloc a block
         * @return  Non-Null pointer on success, Null otherwise
         */
		void* alloc();
    
        /**
         * Free a block
         * @return  True on success, false otherwise
         */
		bool free(void* ptr);
    
	private:
        // Clear memory on free
		bool m_clearOnFree;

        // Block size
		const unsigned int m_blockSize;
        
        // Number of blocks
		const unsigned int m_numBlocks;
    
        // Number of states
		const unsigned int m_numStates;

        // State information (1 - allocated, 0 - free)
		unsigned int* m_states;
    
        // Memory pool
		unsigned char* m_memory;
	};

    /**
     * Dynamically-sized memory pool
     * Allows allocations of any size
     * Faster allocation, slower free, memory overhead for management, fragmentation
     */
	class DynamicPool
	{
	public:
        /**
         * Constructor
         * @param size          Size of the pool
         * @param defragOnFree  Perform a defragmention step on free
         * @param clearOnFree   Clear memory on free
         */
		DynamicPool(unsigned int size, bool defragOnFree = true, bool clearOnFree = false);
        
        /**
         * Deconstructor
         */
		~DynamicPool();
        
        /**
         * Allocate a block of memory
         * @param size  Size of the block
         * @return      Non-Null on success, false otherwise
         */
		void* alloc(unsigned int size);

        /**
         * Free a block of memory
         * @param ptr   Block to free
         * @return      True on success, false otherwise
         */
		bool free(void* ptr);
        
        /**
         * Defragment the memory pool
         * @param steps     Number of defragmentation steps. -1 to run until completely defragmented.
         */
        void defragment(int steps = -1);

	private:
        /**
         * Allocation management information
         */
#if defined(__APPLE__)
		struct __attribute__((packed)) Header
#elif defined(WIN32)
		__pragma(pack(push, 1)) struct Header 
#endif
		{
			unsigned int used:1;    // Allocated
			unsigned int size:23;   // Block size - If last, 0
			unsigned int offset:23; // Offset from the front
			unsigned int last:1;    // If the block is the closest to the header table
		};
#if defined(WIN32)
		__pragma(pack(pop))
#endif

        // Clear block on free
		bool m_clearOnFree;

        // Perform a defragmentation step on free
        bool m_defragOnFree;
        
        // Pool size
		unsigned int m_size;

        // Number of fragments
		unsigned int m_numFragments;

        // Memory pool
		unsigned char* m_memory;
        
        // Last fragment - closest to the header table
        Header* m_last;
        
        /**
         * Makes one iteration over the fragment list, 
         * merging adjacent, free fragments.
         * @return True if blocks were merged, false otherwise
         */
        bool defragmentStep();
	};

    /**
     * Constructor
     */
	Memory();
    
    /**
     * Deconstructor
     */
	~Memory();

    /**
     * Add a BlockPool
     * @param pool  BlockPool
     * @param own   Give ownership to the Memory instance.
     *              The Memory instance will delete it on deconstruction.
     */
	void addPool(BlockPool* pool, bool own = false);

    /**
     * Add a DynamicPool
     * @param pool  DynamicPool
     * @param own   Give ownership to the Memory instance.
     *              The Memory instance will delete it on deconstruction.
     */
    void addPool(DynamicPool* pool, bool own = false);
    
    /**
     * Attempt to allocate a block of memory
     * @param size  Memory block size
     * @return      Non-Null on success, Null otherwise
     */
	void* alloc(unsigned int size);

    /**
     * Free a memory block
     * @param ptr   Memory block
     */
	void free(void* ptr);
    
    /**
     * Create a managed large block of memory
     * @param id    Block Id
     * @param size  Size of the block
     * @return      True on success, false otherwise
     */
    bool createBigBlock(unsigned int id, unsigned int size);
    
    /**
     * Allocate/take a large block of memory
     * @param id    Block Id
     * @return      Non-Null on success, Null otherwise
     */
    void* allocBigBlock(unsigned int id);
    
    /**
     * Free/return a large block of memory
     * @param id    Block Id
     */
    void freeBigBlock(unsigned int id);

private:
    /**
     * BlockPool container
     */
	struct BlockPoolContainer
	{
        // Block size
		unsigned int blockSize;
        // If it is owned by Memory
		bool owned;
        // BlockPool instance
		BlockPool* pool;
	};

    /**
     * DynamicPool container
     */
    struct DynamicPoolContainer
	{
        // If it is owned by Memory
		bool owned;
        // UnsizePool instance
		DynamicPool* pool;
	};
    
    /**
     * Big (Memory) Block container
     */
    struct BigBlockContainer
    {
        // Size of the block
        unsigned int size;
        // Allocated or free
        bool allocated;
        // Memory
        unsigned char* memory;
    };
    
    // List of BlockPools
	BlockPoolContainer* m_blockPools;
    
    // Number of BlockPools
	unsigned int m_numBlockPools;
    
    // List of DynamicPools
	DynamicPoolContainer* m_dynamicPools;
    
    // Number of DynamicPools
	unsigned int m_numDynamicPools;
    
    // List of Big Blocks
    BigBlockContainer* m_bigBlocks;
    
    // Number of Big Blocks
    unsigned int m_numBigBlocks;
};

#endif
