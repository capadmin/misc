//
//  timer.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/19/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef TIMER_H
#define TIMER_H

#if !defined(DISABLE_TIMER)

#include "log.h"

#if !defined(TIMER_ACCURACY)
#define	TIMER_ACCURACY	'N'
#endif

#if (TIMER_ACCURACY == 'N')

#define	TIMER_FORMAT()	"%d ns"
#define TIMER_FUNC(NAME)		NAME.nseconds()

#elif (TIMER_ACCURACY == 'M')

#define	TIMER_FORMAT	"%d ms"
#define TIMER_FUNC		.mseconds()

#elif (TIMER_ACCURACY == 'S')

#define TIMER_FORMAT	"%f s"
#define TIMER_FUNC(NAME)		NAME##.seconds()

#endif

// Scoped Timer
// Times a scope
// Useful for timing an entire function or loop iterations.
// Waiting for deconstruction adds a not-insignificant overhead. Most noticeable in smaller times.
#define TIMER_SCOPED( NAME ) \
	Timer ScopedTimerNAME##Timer( #NAME, true )

// Block Timer
// Times a segment of code, between the Start and Stop calls

// Block Timer Start
#define TIMER_START( NAME ) \
    Timer BlockTimer##NAME##Timer( #NAME ); \
    BlockTimer##NAME##Timer.start();

// Block Timer Stop
#define TIMER_STOP( NAME ) \
	BlockTimer##NAME##Timer.stop(); \
    LOGF("%s - " TIMER_FORMAT() "\n", BlockTimer##NAME##Timer.name(), TIMER_FUNC(BlockTimer##NAME##Timer));

// Accumulating Timer
// Times a segment of code, between the Start and Stop calls,
// and adds the timer to the Timer's total.
// The total and average time is reported when Report is called.

// Accumulating Timer Declaration
#define TIMER_ACCUM( NAME ) \
    Timer AccumulatingTimer##NAME##Timer(#NAME); \
    unsigned int AccumulatingTimer##NAME##Total = 0; \
	unsigned int AccumulatingTimer##NAME##Runs = 0;

// Accumulating Timer Start
#define TIMER_ACCUM_START( NAME ) \
	AccumulatingTimer##NAME##Timer.start();

// Accumulating Timer Stop
#define TIMER_ACCUM_STOP( NAME ) \
    AccumulatingTimer##NAME##Timer.stop(); \
    AccumulatingTimer##NAME##Total += TIMER_FUNC(AccumulatingTimer##NAME##Timer); \
	++AccumulatingTimer##NAME##Runs;

// Accumulating Timer Report
#define TIMER_ACCUM_REPORT( NAME ) \
    LOGF("%s - " TIMER_FORMAT() "(%.02f ms)\n", \
		AccumulatingTimer##NAME##Timer.name(), \
		AccumulatingTimer##NAME##Total, \
		(double)AccumulatingTimer##NAME##Total / (double)AccumulatingTimer##NAME##Runs \
	);



// Apple - OSX, iOS
#if defined(__APPLE__)

#include <mach/mach_time.h>

class TimerApple
{
public:
	void start() { m_start = mach_absolute_time(); }
	void stop() { m_stop = mach_absolute_time(); }

	unsigned long long ticks()
	{
		return (m_stop - m_start);
	}
    
    unsigned int nseconds()
    {
        static mach_timebase_info_data_t s_timebaseInfo;
        if ( s_timebaseInfo.denom == 0 )
            mach_timebase_info(&s_timebaseInfo);
        
        return (unsigned int)((m_stop - m_start) * s_timebaseInfo.numer / s_timebaseInfo.denom);
    }
    
	unsigned int mseconds()
    {
        return nseconds() / 1000000;
    }
    
	double seconds()
    {
        return (double)nseconds() / 1000000000.0;
    }
    
protected:    
	uint64_t m_start;
	uint64_t m_stop;
};

// Windows
#elif defined(WIN32)

#include <windows.h>

class TimerWin32
{
public:
	void start() { QueryPerformanceCounter(&m_start); }
	void stop() { QueryPerformanceCounter(&m_stop); }
    
	unsigned long long ticks()
	{
		return m_stop.QuadPart - m_start.QuadPart;
	}
    
    unsigned int nseconds()
    {
        return (unsigned int)(seconds() * 1000000000.0);
    }
    
	unsigned int mseconds()
    {
        return (unsigned int)(seconds() * 1000.0);
    }
    
	double seconds()
    {
        static LARGE_INTEGER s_frequency;
		if( s_frequency.QuadPart == 0 )
            QueryPerformanceFrequency(&s_frequency);
		
		return (double)(m_stop.QuadPart - m_start.QuadPart) / (double)s_frequency.QuadPart;
    }
    
protected:    
	LARGE_INTEGER m_start;
	LARGE_INTEGER m_stop;
};

// Everything else Posix-compatible (Linux, Android, etc.)
#else

#include <time.h>

typedef timespec timer_t;

class TimerPosix
{
public:
	void start() { clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &m_start); }
	void stop() { clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &m_stop); }
    
	unsigned long long ticks()
	{
		return 0; // No ticks available
	}
    
    unsigned int nseconds()
    {
		return m_stop.tv_nsec - m_start.tv_nsec;
    }
    
	unsigned int mseconds()
    {
		return (m_stop.tv_nsec - m_start.tv_nsec) / 1000000;
    }
    
	double seconds()
    {
		//m_stop.tv_sec - m_start.tv_sec;
        return (double)(m_stop.tv_nsec - m_start.tv_nsec) / 1000000000.0;
    }
    
protected:
	timespec m_start;
	timespec m_stop;
};

#endif

/**
 * Timer
 * Holds a name and tracks elapsed time
 */
#if defined(__APPLE__)
class Timer : public TimerApple
#elif defined(WIN32)
class Timer : public TimerWin32
#else
class Timer : public TimerPosix
#endif
{
public:
	Timer( const char* name = NULL, bool scoped = false )
    : m_name(name)
    , m_scoped(scoped)
	{
        if( m_scoped )
            start();
	}

	~Timer()
	{
		if( m_scoped )
		{
			stop();
			LOGF("%s - " TIMER_FORMAT() "\n", m_name, TIMER_FUNC((*this)));
		}
	}

	/**
     * Get the name
     */
    const char* name() { return m_name; }

	/**
     * Set the start mark for the timer
     */
	//void start();

    /**
     * Set the stop mark for the timer
     */
	//void stop();
    
    /**
     * Returns the number of clock ticks elapsed
     */
	//unsigned long long ticks();
    
    /**
     * Returns the number of nanoseconds elapsed
     */
    //unsigned int nseconds();
    
    /**
     * Returns the number of milliseconds elapsed
     */
	//unsigned int mseconds();
    
    /**
     * Returns the number of seconds elapsed
     */
	//double seconds();
    
private:
    // Name
	const char* m_name;
    
    // Is scroped - should call stop and report on deconstruction
    bool m_scoped;

//protected:
	// Start mark
	//type m_start;
    
    // Stop mark
	//type m_stop;
};

#else

// Undefined/Disable all timing

#define TIMER_SCOPED( NAME )
#define TIMER_START( NAME )
#define TIMER_STOP( NAME )
#define TIMER_ACCUM( NAME )
#define TIMER_ACCUM_START( NAME )
#define TIMER_ACCUM_STOP( NAME )
#define TIMER_ACCUM_END( NAME )

#endif

#endif