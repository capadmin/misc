//
//  config_manager.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 11/16/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef config_manager_h
#define config_manager_h

#include <map>
#include <vector>
#include <cassert>

#include "hash.h"
#include "string_pool.h"

class ConfigManager
{
public:
	class Config
	{
	public:
		typedef std::vector<Config> ConfigArray;
		
		/**
		 * Raw Value data
		 */
		class ValueData
		{
		public:
			
			// Data type
			enum Type
			{
				eINVALID,
				eINTEGER,	//Integer
				eFLOAT,		//Float
				eSTRING,	//String Offset
				eCHILD,		//Child Index
			};
			
			// Data
			union
			{
				// Integer
				int32_t i;
				// Float
				float f;
				// String offset & Child index
				uint32_t o;
			} m_value;
			
			// Type
			Type m_type;
			
			/**
			 * Set integer value
			 * @param v	Integer value
			 */
			void set(int32_t v) { m_value.i = v; m_type = eINTEGER; }
			
			/**
			 * Set float value
			 * @param v	Float value
			 */
			void set(float v) { m_value.f = v; m_type = eFLOAT; }
			
			/**
			 * Set offset/index value
			 * @param o		Offset/Index value
			 * @param type	Type
			 */
			void set(uint32_t o, Type type) { m_value.o = o; m_type = type; }
		};
		
		/**
		 * Value Container
		 * Interface to Value data
		 */
		class Value
		{
		public:
			// Invalid Value
			static const Value INVALID;
			
			/**
			 * Constructor
			 */
			Value()
			: m_data(nullptr)
			{
				m_value.c = nullptr;
			}
			
			/**
			 * Get value type
			 */
			ValueData::Type type() const { return m_data == nullptr ? ValueData::eINVALID : m_data->m_type; }
			
			/**
			 * Integer value operator
			 */
			operator int32_t() const
			{ assert(m_data != nullptr); assert(m_data->m_type != ValueData::eINVALID); return m_data->m_type == ValueData::eFLOAT ? (int32_t)m_data->m_value.f : m_data->m_value.i; }
			
			/**
			 * Float value operator
			 */
			operator float() const
			{ assert(m_data != nullptr); assert(m_data->m_type != ValueData::eINVALID); return m_data->m_type == ValueData::eFLOAT ? m_data->m_value.f : (float)m_data->m_value.i; }
			
			/**
			 * Offset/Index value operator
			 */
			operator uint32_t() const
			{ assert(m_data != nullptr); assert(m_data->m_type != ValueData::eINVALID); assert(m_data->m_type == ValueData::eCHILD); return m_data->m_value.o; }
			
			/**
			 * String value operator
			 */
			operator const char*() const
			{ assert(m_data != nullptr); assert(m_data->m_type != ValueData::eINVALID); assert(m_data->m_type == ValueData::eSTRING); return m_value.s; }
			
			/**
			 * Child Config value operator
			 */
			operator const Config*() const
			{ assert(m_data != nullptr); assert(m_data->m_type != ValueData::eINVALID); assert(m_data->m_type == ValueData::eCHILD); return m_value.c; }
			
			/**
			 * Child Config value operator
			 */
			operator const Config&() const
			{ assert(m_data != nullptr); assert(m_data->m_type != ValueData::eINVALID); assert(m_data->m_type == ValueData::eCHILD); return *m_value.c; }
			
		private:
			
			/**
			 * Constructor
			 * @param	data		Value data
			 * @param	children	Child config list
			 * @param	pool		String pool
			 */
			Value(const ValueData* data, const ConfigArray& children, const DynamicStringPool& pool)
			: m_data(data)
			{
				if( m_data->m_type == ValueData::eSTRING )
					m_value.s = pool.getString(m_data->m_value.o);
				else if( m_data->m_type == ValueData::eCHILD )
					m_value.c = &children[m_data->m_value.o];
				else
					m_value.s = nullptr;
			}
			
			// Value data
			const ValueData* m_data;

			// Extended value data
			union
			{
				// String
				const char* s;
				// Config
				const Config* c;
			} m_value;
			
			friend class Config;
		};
		
		typedef std::map<hash_t, ValueData> ValueMap;
		
		/**
		 * Constructor
		 */
		Config();
		
		/**
		 * Destructor
		 */
		~Config();
		
		/**
		 * Is empty - contains no values
		 */
		bool isEmpty() const { return m_values.empty(); }
		
		/**
		 * Contains a value with the specified key
		 * @param name	Name key
		 * @return		True if the key'd value exists, false otherwise
		 */
		bool contains(hash_t name) const { return m_values.find(name) != m_values.end(); }
		
		/**
		 * Contains a value with the specified key
		 * @param name	Name key
		 * @return		True if the key'd value exists, false otherwise
		 */
		bool contains(const char* name) const { return contains(hash(name)); }
		
		/**
		 * Get value by name
		 * @param name	Name key
		 * @return		Value if found, Invalid value otherwise
		 */
		Value value(hash_t name) const;
		
		/**
		 * Get value by name
		 * @param name	Name key
		 * @return		Value if found, Invalid value otherwise
		 */
		Value value(const char* name) const { return value(hash(name)); }
		
		/**
		 * Get child config by name
		 * @param name	Name key
		 * @return		Config
		 */
		const Config& child(hash_t name) const;
		
		/**
		 * Get child config by name
		 * @param name	Name key
		 * @return		Config
		 */
		const Config& child(const char* name) const { return child(hash(name)); }
		
		/**
		 * Add integer value
		 * @param name	Name key
		 * @param value	Integer value
		 * @return		True on success, false otherwise
		 */
		bool addValue(hash_t name, int32_t value);
		
		/**
		 * Add float value
		 * @param name	Name key
		 * @param value	Float value
		 * @return		True on success, false otherwise
		 */
		bool addValue(hash_t name, float value);
		
		/**
		 * Add string value
		 * @param name	Name key
		 * @param value	String value
		 * @return		True on success, false otherwise
		 */
		bool addValue(hash_t name, const char* value);
		
		/**
		 * Add child config
		 * @param name	Name key
		 * @return		Child config
		 */
		Config& addChild(hash_t name);
		
		/**
		 * Provide the number of values that will be added
		 * Prevents multiple allocations during building
		 * @param num	Number of values
		 */
		void hintNumValues(size_t num);
		
		/**
		 * Provide the number of child configs that will be added
		 * Prevents multiple allocations during building
		 * @param num	Number of children
		 */
		void hintNumChildren(size_t num);
		
	private:
		// Map of values - key: hash_t, value: ValueData
		ValueMap m_values;
		
		// List of Configs
		ConfigArray m_children;
		
		// String pool
		DynamicStringPool m_pool;
	};
};

#endif
