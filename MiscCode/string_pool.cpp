//
//  string_pool.cpp
//  MiscCode
//
//  Created by Morgenroth, Kyle on 11/16/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#include "string_pool.h"

#include <cstring>

#include "memory.h"

StringPoolBase::StringPoolBase(unsigned int size)
: m_pool(size > 0 ? new char[size] : nullptr)
, m_size(size)
, m_length(0)
, m_static(size != 0)
{
}

StringPoolBase::~StringPoolBase()
{
	DELETE_SAFE(m_pool);
}

const char* StringPoolBase::addString(const char* str, int length)
{
	if( str == nullptr )
		return nullptr;
	
	if( length == -1 )
		length = (int)strlen(str);

	if( m_pool != nullptr )
	{
		const char* ptr = getString(str, length);
		if( ptr != nullptr )
			return ptr;
	}
	
	++length; // Include \0
	
	if( m_size - m_length < length )
	{
		if( m_static )
			return nullptr;
		
		m_size += (length + 50);
		
		char* newPool = new char[m_size];
		
		memcpy(newPool, m_pool, m_length);
		DELETE_SAFE(m_pool);
		
		m_pool = newPool;
	}
	
	char* ptr = m_pool + m_length;
	memcpy(ptr, str, length);
	m_length += length;
		
	return ptr;
}

const char* StringPoolBase::getString(const char* str, int length) const
{
	if( m_pool == nullptr )
		return nullptr;
	
	if( str == nullptr )
		return nullptr;
	
	if( length == -1 )
		length = (int)strlen(str);
	++length;
	
	return (const char*)memmem(m_pool, m_length, str, length);
}

void StringPoolBase::compact()
{
	if( m_pool == nullptr )
		return;
	
	if( m_size == m_length )
		return;
	
	if( m_static )
		return;
	
	char* newPool = new char[m_length];
	
	memcpy(newPool, m_pool, m_length);
	DELETE_SAFE(m_pool);
	
	m_pool = newPool;
	m_size = m_length;
}
