//
//  memory_pool.inl
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/24/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#include "memory_pool.h"

#include <string.h>

#include "bits.h"

Memory::Pool::Pool(unsigned int size, unsigned int num, bool clearOnFree)
: m_clearOnFree(clearOnFree)
, m_blockSize(size)
, m_numBlocks(num < 32 ? 32 : nextPow2(num))
, m_numStates(m_numBlocks >> 5)
, m_states(NULL)
, m_memory(NULL)
{
	const unsigned int numStates = m_numStates + 1;

    m_states = new unsigned int[numStates];
    memset(m_states, 0, sizeof(unsigned int) * numStates);
    
    m_memory = new unsigned char[m_numBlocks * m_blockSize];
}

Memory::Pool::~Pool()
{
    DELETE_SAFE_ARR(m_states);
    DELETE_SAFE_ARR(m_memory);
}

unsigned int Memory::Pool::blockSize()
{
	return m_blockSize;
}

void* Memory::Pool::alloc()
{
    // There are always an empty state at the end
	unsigned int* states = m_states;
    while( *states == 0xFFFFFFFF )
		++states;
    
	const unsigned int stateIndex = states - m_states;
    if( stateIndex >= m_numStates )
		return NULL;

    // We know there is an unset bit somewhere
	const unsigned int state = *states;
    unsigned int bitIndex = 0;
    while((state & (1 << bitIndex)) > 0)
        ++bitIndex;
    
    *states |= (1 << bitIndex);
    
    const unsigned int blockIndex = ((stateIndex << 5) | bitIndex);
    
    return m_memory + (blockIndex * m_blockSize);
}

bool Memory::Pool::free(void* ptr)
{
	const unsigned int blockIndex = ((unsigned char*)ptr - m_memory) / m_blockSize;
	if( blockIndex >= m_numBlocks )
		return false;

	const unsigned int stateIndex = blockIndex >> 5;
	const unsigned int bitIndex = blockIndex & 32;

#if defined(DEBUG)
	if( (m_state[stateIndex] & (1 << bitIndex)) == 0 )
		ERRORF("Block [%d] is already free!", blockIndex);

	if( m_clearOnFree )
		memset(ptr, 0xFE, m_blockSize);
#endif

	m_states[stateIndex] &= ~(1 << bitIndex);

	return true;
}
