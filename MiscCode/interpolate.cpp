//
//  interpolate.cpp
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/9/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#include "interpolate.h"

#include <math.h>

namespace Interpolate
{

/**
 * Simple linear interpolation between a start and end value
 * @param   start   Start value
 * @param   end     End value
 * @param   t       Normalized distance from start, [0, 1]
 * @return          Interpolated value between start and end
 */
float linear(float start, float end, float t)
{
    // Determine the length traveled between start and end
    // represented by the t-value.
    // Add to start to determine the new value.
    return start + ((start - end) * t);
}
   
float linear(float speed, float value, float max, float delta)
{
    // Apply delta value to value
    // return the lowest value between value and max
    value += speed * delta;
    
    return (value < max ? value : max);
}

/**
 * Sine-based interpolation
 * Eases towards the maximum value quickly, and coasts to it
 * @param   value   Current value
 * @param   end     End value
 * @param   delta   Distance moved towards the end value [0, 1]
 * @return          Interpolated value
 */
float sine(float value, float end, float delta)
{
    // asinf takes the y-value ([0, 1]) as the parameter and returns
    // the x-value ([0, PI/2])
    // Add the delta value ([0, 1]) in the space of the x-value by
    // multiplying it by PI/2.
    // sinf takes the modify x-value, returns the new y-value
    // and that is multiplied by the end value to generate the
    // new value.
    return sinf(asinf(value / end) + (delta * M_PI_2)) * end;
}

/**
 * Power-based interpolation
 * Eases in, and then accelerates towards the maximum value very quickly
 * @param   value   Current value
 * @param   end     End value
 * @param   delta   Distance moved towards the end value [0, 1]
 * @return          Interpolated value
 */
float power(float value, float end, float delta)
{
    // sqrtf takes the y-value ([0, 1]) and returns the x-value ([0, 1])
    // Add the delta value ([0, 1]) to the x-value, square it, and
    // multiple by the end value to generate the new value.
    float x = sqrtf(value / end) + delta;
    return (x * x) * end;
}
    
}