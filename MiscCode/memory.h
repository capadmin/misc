//
//  memory.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/23/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef memory_h
#define memory_h

// Safe Free
#define DELETE_SAFE( ptr ) { delete ptr; ptr = nullptr; }

// Safe Array Free
#define DELETE_SAFE_ARR( ptr ) { delete [] ptr; ptr = nullptr; }

// Safe Itor Delete
#define DELETE_SAFE_ITOR( arr ) { const auto itorEnd = map.end(); for( auto itor = map.begin(); itor != itorEnd; ++itor ) DELETE_SAFE(*itor); }

// Safe Map Delete
#define DELETE_SAFE_MAP( map) { const auto itorEnd = map.end(); for( auto itor = map.begin(); itor != itorEnd; ++itor ) DELETE_SAFE(itor->second); }

/**
 * (F)ast (Alloc)ation
 * Attempts to allocate memory on the stack, via alloca, if it under the specified threshold.
 * If it exceeds the threshold, it allocs it from the heap.
 * To ensure no memory leaks, all falloc() calls must be paired with a ffree() call.
 */
#if !defined(DISABLE_FALLOC) // Custom Alloca

#define FALLOC_THRESHOLD	1024
#define FALLOC_MARKER_TYPE	unsigned int
#define FALLOC_MARKER_SIZE	4
#define FALLOC_MARK_S		0xAAAAAAAA
#define FALLOC_MARK_H		0xBBBBBBBB

#if defined(WIN32)

#include <malloc.h>

#define ___alloca	_alloca

#elif defined(__APPLE__)

#include <alloca.h>

#define ___alloca	alloca

#endif

inline void* _markAlloc( void* mem, FALLOC_MARKER_TYPE mark )
{
	*((FALLOC_MARKER_TYPE*)mem) = mark;
	return (void*)(((char*)mem) + FALLOC_MARKER_SIZE);
}

#define falloc(x) _markAlloc( (((x) + FALLOC_MARKER_SIZE) <= FALLOC_THRESHOLD ? ___alloca( ((x) + FALLOC_MARKER_SIZE) ) : new char[((x) + FALLOC_MARKER_SIZE)]), (((x) + FALLOC_MARKER_SIZE) <= FALLOC_THRESHOLD ? FALLOC_MARK_S : FALLOC_MARK_H) );
#define ffree(x) ( *(((FALLOC_MARKER_TYPE*)(x)) - 1) == FALLOC_MARK_S ? (void)x : delete [] (char*)(((FALLOC_MARKER_TYPE*)(x)) - 1) );

#elif !defined(DISABLE_ALLOCA)	// Disable Falloca and replace with alloca

#if defined(WIN32)

#include <malloc.h>

#define falloc		_alloca
#define ffree(x)	(void)x;

#elif defined(__APPLE__)

#include <alloca.h>

#define falloc		alloca
#define ffree(x)	(void)x;

#endif

#else  // Standard malloc/free

#define falloc(x)	((void*)(new char[x]))
#define ffree(x)	(delete [] x)

#endif

#endif
