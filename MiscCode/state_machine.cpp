//
//  state_machine.cpp
//  MiscCode
//
//  Created by Morgenroth, Kyle on 11/8/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#include "state_machine.h"

#include "memory.h"

//// StateMachine::State

StateMachine::State::State(const char* name)
: StateMachine::State::State(hash(name))
{
}

StateMachine::State::State(hash_t name)
: m_name(name)
, m_next(nullptr)
, m_transitions(nullptr)
{
}

StateMachine::State::~State()
{
	if( m_transitions != nullptr )
	{
		Transition* transition = nullptr;
		while( m_transitions != nullptr )
		{
			transition = m_transitions->m_next;
			DELETE_SAFE(m_transitions);
			m_transitions = transition;
		}
	}
}

bool StateMachine::State::addTransition(StateMachine::Transition* transition)
{
	if( transition == nullptr )
		return false;
	
	transition->m_next = m_transitions;
	m_transitions = transition;
	
	return true;
}

StateMachine::Transition* StateMachine::State::post(const StateMachine::Event& event)
{
	if( m_transitions == nullptr )
		return nullptr;
	
	Transition* transition = m_transitions;
	
	do
	{
		if( transition->activates(event) )
			return transition;
		
		transition = transition->m_next;
	}
	while (transition != nullptr);
	
	return nullptr;
}

//// State Machine

StateMachine::StateMachine()
: m_states(nullptr)
, m_currentState(nullptr)
, m_currentTransition(nullptr)
{
}

StateMachine::~StateMachine()
{
	m_currentState = nullptr;
	m_currentTransition = nullptr;
	
	if( m_states != nullptr )
	{
		State* state = nullptr;
		while( m_states != nullptr )
		{
			state = m_states->m_next;
			DELETE_SAFE(m_states);
			m_states = state;
		}
	}
}

bool StateMachine::addState(StateMachine::State* state)
{
	if( state == nullptr )
		return false;
	
	state->m_next = m_states;
	m_states = state;
	
	if( m_currentState == nullptr )
		setState(state->name());
	
	return true;
}

bool StateMachine::post(const StateMachine::Event& event)
{
	if( m_currentState == nullptr )
		return false;
	
	if( m_currentTransition != nullptr )
		return false;
	
	Transition* transition = m_currentState->post(event);
	if( transition != nullptr )
		transit(transition);
	
	return true;
}

bool StateMachine::setState(hash_t name)
{
	if( m_states == nullptr )
		return false;
	
	State* state = m_states;
	
	do
	{
		if( state->name() == name )
			break;
		
		state = state->m_next;
	}
	while( state != nullptr );
	
	if( state == nullptr )
		return false;
	
	if( m_currentTransition != nullptr )
		m_currentTransition->end();
	m_currentTransition = nullptr;
	
	if( m_currentState != nullptr )
		m_currentState->exit();
	
	m_currentState = state;
	m_currentState->enter();
	
	return true;
}

void StateMachine::update()
{
	if( m_currentTransition != nullptr )
	{
		if( !m_currentTransition->update() )
			setState(m_currentTransition->toState());
	}
	else if( m_currentState != nullptr )
	{
		Transition* transition = m_currentState->update();
		if( transition != nullptr )
			transit(transition);
	}
}

void StateMachine::transit(StateMachine::Transition* transition)
{
	if( transition->isImmediate() )
	{
		setState(transition->toState());
	}
	else
	{
		m_currentState->exit();
		m_currentState = nullptr;
		
		m_currentTransition = transition;
		m_currentTransition->begin();
	}
}
