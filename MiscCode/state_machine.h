//
//  state_machine.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 11/6/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef state_machine_h
#define state_machine_h

#include <cstdint>

#include "hash.h"

#include "log.h"

/**
 * State Machine
 */
class StateMachine
{
public:
	
	/**
	 * Event
	 * Relays changes to the StateMachine and may
	 * trigger a transition between states
	 */
	class Event
	{
	public:
		/**
		 * Constructor
		 * @param	id		Event id
		 * @param	name	Event name
		 */
		Event(uint16_t id, hash_t name)
		: m_id(id)
		, m_name(name)
		{ }
		
		/**
		 * Get id
		 */
		uint16_t id() const { return m_id; }
		
		/**
		 * Get name
		 */
		hash_t name() const { return m_name; }
		
	protected:
		// Id
		uint16_t m_id;
		
		// Name
		hash_t m_name;
	};

	
	/**
	 * Transition
	 * A change from one state to another, triggered
	 * by an event.
	 * A transition can be immediate, and occur instantly, or not.
	 * If not, begin() will be called when it begins. update() will be
	 * called once per state-machine update until it returns false. Then,
	 * end() will be called and the new state will be entered.
	 */
	class Transition
	{
	public:
		/**
		 * Constructor
		 * @param eventId	Id of the event to trigger the transition
		 * @param eventName	Name of the event to trigger the transiton
		 * @param fromState	Name of the state to transition from
		 * @param toState	Name of the state to transition to
		 */
		Transition(uint16_t eventId, hash_t eventName, hash_t fromState, hash_t toState)
		: m_eventId(eventId)
		, m_eventName(eventName)
		, m_fromState(fromState)
		, m_toState(toState)
		{ }

		/**
		 * Constructor
		 * @param eventId	Id of the event to trigger the transition
		 * @param eventName	Name of the event to trigger the transiton
		 * @param fromState	Name of the state to transition from
		 * @param toState	Name of the state to transition to
		 */
		Transition(uint16_t eventId, hash_t eventName, const char* fromState, const char* toState)
		: Transition(eventId, eventName, hash(fromState), hash(toState))
		{ }
		
		/**
		 * Does the transition occur immediately
		 */
		virtual bool isImmediate() const { return true; }
		
		/**
		 * Name of the State that is transitioned from
		 */
		uint32_t fromState() const { return m_fromState; }
		
		/**
		 * Name of the state that is transitioned to
		 */
		uint32_t toState() const { return m_toState; }
		
		/**
		 * Check if the event triggers the transition
		 * @param event	Event
		 * @return		True if the transition is triggered, false otherwise
		 */
		virtual bool activates(const Event& event) const
		{
			return (m_eventId == 0 || event.id() == m_eventId) && (m_eventName == 0 || event.name() == m_eventName);
		}
		
		/**
		 * Non-immediate transition is beginning
		 */
		virtual void begin() { }
		
		/**
		 * Non-immediate transition is ending
		 */
		virtual void end() { }
		
		/**
		 * Non-immediate transition is updating
		 * @return	True if the transition is complete, false otherwise
		 */
		virtual bool update() { return true; }
		
	protected:
		// Triggering event's id
		uint16_t m_eventId;
		
		// Triggering event's name
		hash_t m_eventName;
		
		// From-State's name
		hash_t m_fromState;
		
		// To-State's name
		hash_t m_toState;
		
	private:
		// List next
		Transition* m_next;
		
		friend class StateMachine;
	};
	
	/**
	 * State
	 */
	class State
	{
	public:
		/**
		 * Constructor
		 * @param name	Name
		 */
		State(hash_t name);

		/**
		 * Constructor
		 * @param name	Name
		 */
		State(const char* name);
		
		/**
		 * Destructor
		 */
		virtual ~State();

		/**
		 * Get name
		 */
		hash_t name() const { return m_name; }

		/**
		 * Add a transition from this state to another
		 * @param transition	Transition
		 * @return				True on success, false otherwise
		 */
		bool addTransition(Transition* transition);
		
		/**
		 * Post an event to the state
		 * @param event		Event to post
		 * @return			The transition triggered, or null
		 */
		Transition* post(const Event& event);
		
		/**
		 * Enter the state
		 */
		virtual void enter()
		{
#if defined(DEBUG)
			INFOF("State [%s] - Enter", unhash(m_name));
#endif
		}
		
		/**
		 * Exit the state
		 */
		virtual void exit()
		{
#if defined(DEBUG)
			INFOF("State [%s] - Exit", unhash(m_name));
#endif
		}
		
		/**
		 * Update the state
		 * @return	Transition, if the state should change, null otherwise
		 */
		virtual Transition* update() { return nullptr; }
		
	protected:
		// Name
		hash_t m_name;
		
		// Transitions
		Transition* m_transitions;
		
	private:
		// List next
		State* m_next;

		friend class StateMachine;
	};
	
	/**
	* Constructor
	*/
	StateMachine();
	
	/**
	* Destructor
	*/
	~StateMachine();

	/**
	* Get current state
	*/
	const State* currentState() const { return m_currentState; }

	/**
	* Get current non-immediate transition
	*/
	const Transition* currentTransition() const { return m_currentTransition; }
	
	/**
	* Add a state
	* The first state to be added is immediately set as the
	* current state
	* @param state	State
	* @return		True on success, false otherwise
	*/
	bool addState(State* state);
	
	/**
	* Post an event to the state machine
	* @param event	Event
	* @return		True on success, false otherwise
	*/
	bool post(const Event& event);
	
	/**
	* Set the current state
	* Unsets the current state or stops the current transition
	* @param name	State name
	* @return		True on success, false otherwise
	*/
	bool setState(hash_t name);
	
	/**
	* Update state machine
	*/
	void update();
	
private:
	// State list
	State* m_states;
	
	// Current state
	State* m_currentState;

	// Current transition
	Transition* m_currentTransition;
	
	/**
	* Peform a transition
	* @param transition	Transition
	*/
	void transit(Transition* transition);
};


#endif
