//
//  main.cpp
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/6/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>

#include "interpolate.h"

#include "memory_pool.h"
#include "string_pool.h"

#include "entity.h"

#include "state_machine.h"
#include "state_machine_ext.h"

#include "parts_manager.h"

#include "timer.h"
#include "log.h"

void string_pool_test()
{
	StaticStringPool pool(2048);
	
	const char* a = pool.addString("Test");
	assert(a != nullptr);
	const char* b = pool.addString("Test");
	assert(a == b);
	const char* c = pool.addString("t");
	assert((a + 3) == c);
	const char* d = pool.addString("Blarg");
	assert((a + 5) == d);
	
}

void state_machine_test()
{
	StateMachine machine;
	
	StateMachine::State* alive = new StateMachine::State("alive");
	
	StateMachine::State* dead = new StateMachine::State("dead");
	
	NumericCompareTransition* transition = new NumericCompareTransition(
		hash("health"),
		alive->name(), dead->name(),
		NumericCompareTransition::eLESS_THAN_EQUAL, 0
	);
	
	alive->addTransition(transition);
	
	machine.addState(alive);
	machine.addState(dead);
	
	NumericEvent event50(hash("health"), 50);
	NumericEvent event0(hash("health"), 0);
	
	LOGF("Current State: %s", unhash(machine.currentState()->name()));
	
	LOGF("Post Health(50)");
	machine.post(event50);
	LOGF("Current State: %s", unhash(machine.currentState()->name()));
	
	LOGF("Post Health(0)");
	machine.post(event0);
	LOGF("Current State: %s", unhash(machine.currentState()->name()));
}


#define cimg_display 0
#include "CImg.h"

const unsigned int IMG_SIZE = 1024;
const float IMG_SCALE = 10.0f;

const float IMG_FACTOR = ((IMG_SIZE/2.0f) / IMG_SCALE);

#define P2B(x) (x * ((IMG_SIZE/2.0f) / IMG_SCALE))

vector2 convert(const vector2& p)
{
	return vector2(p.x * IMG_FACTOR + (IMG_SIZE / 2.0f), p.y * -IMG_FACTOR + (IMG_SIZE / 2.0f));
}

Entity entity(1.0f, 1.0f, 1.0f, 15.0f, 0.5f);
cimg_library::CImg<unsigned char> img(IMG_SIZE, IMG_SIZE, 1, 3);

bool entity_setDestination(const vector2& d, float r)
{
	const float turnRadius = entity.maxTurnVelocity() / tanf(entity.maxTurnAngle());
	
	const unsigned char turnColor[3] = { 255, 0, 255 };
	const vector2 a = convert(vector2(-entity.direction().y, entity.direction().x) * turnRadius + entity.position());
	img.draw_circle(a.x, a.y, P2B(turnRadius), turnColor);

	const vector2 b = convert(vector2(entity.direction().y, -entity.direction().x) * turnRadius + entity.position());
	img.draw_circle(b.x, b.y, P2B(turnRadius), turnColor);
	
	const vector2 destination = convert(d);
	const unsigned char destinationColor[3] = { 0, 255, 0 };
	img.draw_circle(destination.x, destination.y, P2B(r), destinationColor);
	
	const vector2 start = convert(entity.position());
	const unsigned char startColor[3] = { 0, 0, 255 };
	img.draw_circle(start.x, start.y, P2B(r), startColor);
	
	return entity.setDestination(d, r);
}

void entity_test()
{
	img.fill(255, 255, 255);

	int steps = 1;

	if( entity_setDestination(vector2(3.75f, 0.0f), 0.1f) )
	{
		while(!entity.update(1 / 30.0f) && steps < 5000)
		{
			const vector2 pixel = convert(entity.position());
			unsigned char speed[3] = { (unsigned char)(255.0f * (entity.velocity() / entity.maxVelocity())), 0, 0 };
			
			img.draw_point<unsigned char>(pixel.x, pixel.y, speed);
            
			LOGF("Step: %d", steps);
			entity.log();
			++steps;
            
			if( steps == 150 )
			{
				entity_setDestination(entity.position() - vector2(0, -1.5f), 0.1f);
			}
			/*
			if( steps == 600 )
            {
                entity_setDestination(vector2(0.0f, -3.8f), 0.1f);
            }

            if( steps == 300 )
            {
                entity_setDestination(vector2(5.0f, 3.8f), 0.1f);
            }
			
			if( steps == 100 )
            {
                entity_setDestination(vector2(-5.0f, 3.8f), 0.1f);
            }
			 */
		}
        
	}
	
	img.save_bmp("/Users/morgk029/Workspaces/MiscCode/MiscCode/path.bmp");
	
	LOGF("Time: %f s", (float)steps * (1 / 30.0f));

}

void memory_pool_block_test()
{
    const unsigned int BLOCKSIZE = 2;
    const unsigned int BLOCKCOUNT = 32;
    Memory::BlockPool blockPool(BLOCKSIZE, BLOCKCOUNT, false);
    
    // Alloc/Free/Alloc
    void* ptr = blockPool.alloc();
    assert(ptr != NULL);
    
    const void* ptrCache = ptr;
    
    blockPool.free(ptr);
    ptr = blockPool.alloc();
    
    assert(ptr == ptrCache);
    
    blockPool.free(ptr);
    
    // Max Allocs
    void* allocs[BLOCKCOUNT];
    int numAllocs = 0;
    while( numAllocs < BLOCKCOUNT && (allocs[numAllocs] = blockPool.alloc()) != NULL )
        ++numAllocs;
    
    assert(numAllocs == BLOCKCOUNT);
    
    // Failed Alloc (Full)
    assert(blockPool.alloc() == NULL);
    
    // Correct Offset
    for( int i = 0; i < numAllocs - 1; ++i )
        assert(((char*)allocs[i] + BLOCKSIZE) == allocs[i+1]);
    
    // No Overlap/Overwrite
    for( int i = 0; i < numAllocs; ++i )
        *((char*)allocs[i]) = (char)i;
    
    for( int i = 0; i < numAllocs; ++i )
        assert(*((char*)allocs[i]) == (char)i);
    
    // Free All
    for( int i = 0; i < numAllocs; ++i )
        assert( blockPool.free(allocs[i]) );
}

void memory_pool_unsized_test()
{
    Memory::DynamicPool pool(1024, false);
    
    void* ptr = pool.alloc(504);
    assert(ptr != NULL);
    
    const void* ptrCache = ptr;
    
    assert( pool.free(ptr) );
    ptr = pool.alloc(504);
    
    assert(ptr == ptrCache);
    
    pool.free(ptr);
    
    // Alloc All (Big)
    void* allocs[1024];
    assert( (allocs[0] = pool.alloc(504)) != NULL );
    assert( (allocs[1] = pool.alloc(504)) != NULL );
    
    // No Overwrite/Overlap
    memset(allocs[0], 0xAA, 504);
    memset(allocs[1], 0xBB, 504);
    
    for( int i = 0; i < 504; ++i )
    {
        assert(((unsigned char*)allocs[0])[i] == 0xAA);
        assert(((unsigned char*)allocs[1])[i] == 0xBB);
    }
    
    // Free All
    assert( pool.free(allocs[0]) );
    assert( pool.free(allocs[1]) );
    
    pool.defragment();
    
    //Memory::UnsizedPool pool2(1024, false);
    
    // Alloc All (Small)
    int numAllocs = 0;
    while( (allocs[numAllocs] = pool.alloc(1)) != NULL )
        ++numAllocs;
    
    assert(numAllocs == 146);
    
    for( int i = 0; i < numAllocs; ++i )
        pool.free(allocs[i]);
    
    pool.defragment();
    
    // Alloc All (Small)
    numAllocs = 0;
    while( (allocs[numAllocs] = pool.alloc(2)) != NULL )
        ++numAllocs;
    
    assert(numAllocs == 128);
}

void timer_test()
{
    float temp;
    TIMER_SCOPED(TEST1);
    
    TIMER_START(TEST2);
    temp = 1000000;
    for( int i = 0; i < 10000000; ++i)
        temp /= 2.0f;
    TIMER_STOP(TEST2);
    
    TIMER_ACCUM(TEST3);
    TIMER_ACCUM_START(TEST3);
    temp = 1000000;
    for( int i = 0; i < 10000000; ++i)
        temp /= 2.0f;
    TIMER_ACCUM_STOP(TEST3);
    TIMER_ACCUM_START(TEST3);
    temp = 1000000;
    for( int i = 0; i < 10000000; ++i)
        temp /= 2.0f;
    TIMER_ACCUM_STOP(TEST3);
    TIMER_ACCUM_REPORT(TEST3);
}

void interpolate_test()
{
    float value;
    float max;
    
    LOGF("Linear w/ Speed");
    
    for( value = 0, max = 1; value < max; )
    {
        value = Interpolate::linear(0.05, value, max, 1);
        LOGF("Value: %.02f\n", value);
    }
    
    LOGF("Linear\n");
    
    for( value = 0, max = -1; value > max; )
    {
        value = Interpolate::linear(-0.05, value, max, 1);
        LOGF("Value: %.02f\n", value);
    }
    
    LOGF("Sine\n");
    
    for( value = 0, max = 10; value < max; )
    {
        value = Interpolate::sine(value, max, 0.05);
        LOGF("Value: %.02f\n", value);
    }
    
    LOGF("Power\n");
    
    for( value = 0, max = 10; value < max; )
    {
        value = Interpolate::power(value, max, 0.05);
        LOGF("Value: %.02f\n", value);
    }
}


extern char* itoa(int value, char* buffer, size_t len, int base);

void itoa_test()
{
    char* buffer1 = NULL;
    size_t size1 = 0;
    
    char buffer2[1];
    size_t size2 = 1;
    
    char buffer3[2];
    size_t size3 = 2;
    
    char buffer4[16];
    size_t size4 = 16;
    
    LOGF("1 - %s\n", itoa(10, buffer1, size1, 10));
    
    LOGF("2 - %s\n", itoa(10, buffer2, size2, 10));
    
    LOGF("3 - %s\n", itoa(1, buffer3, size3, 10));
    
    LOGF("-3 - %s\n", itoa(-1, buffer3, size3, 10));
    
    LOGF("4 - %s\n", itoa(12345, buffer4, size4, 10));
    
    LOGF("4 - %s\n", itoa(-0xFEDCBA, buffer4, size4, 16));
    
    LOGF("4 - %s\n", itoa(0xBEEF, buffer4, size4, 16));
}


int main(int argc, const char * argv[])
{
    //interpolate_test();
    
    //timer_test();
    
    //memory_pool_block_test();
    
    //memory_pool_unsized_test();
    
    //entity_test();
	
	//state_machine_test();
	
	string_pool_test();
    
    return 0;
}


