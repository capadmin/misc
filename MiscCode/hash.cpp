//
//  hash.cpp
//  MiscCode
//
//  Created by Morgenroth, Kyle on 11/10/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#include "hash.h"

#include "platform.h"

// 32-bit magic FNV prime
#if defined(NO_FNV_GCC_OPTIMIZATION)
#define FNV_32_PRIME ((uint32_t)0x01000193)
#endif

#if !defined(DISABLE_UNHASH)

#include <map>

#include "log.h"

typedef std::map<hash_t, const char*> HashMap;

HashMap g_HashMap;

#endif

hash_t hash(const char *str, hash_t hval)
{
	if( str == nullptr || *str == '\0' )
		return 0;
	
    const unsigned char *s = (const unsigned char *)str;
	
    /*
     * FNV-1a hash each octet in the buffer
     */
	do
	{
		/* xor the bottom with the current octet */
		hval ^= (hash_t)*s;
		++s;
		
		/* multiply by the 32 bit FNV magic prime mod 2^32 */
#if defined(NO_FNV_GCC_OPTIMIZATION)
		hval *= FNV_32_PRIME;
#else
		hval += (hval<<1) + (hval<<4) + (hval<<7) + (hval<<8) + (hval<<24);
#endif
    }
	while( *s );
	
#if !defined(DISABLE_UNHASH)
	auto findItor = g_HashMap.find(hval);
	if( findItor != g_HashMap.end() && strcmp(findItor->second, str) != 0 )
		WARNF("Hash Collision - (0x%x) :: %s :: %s", hval, findItor->second, str);
	else
		g_HashMap[hval] = str;
#endif
	
    /* return our new hash value */
    return hval;
}

const char* unhash(hash_t hval)
{
#if !defined(DISABLE_UNHASH)
	auto findItor = g_HashMap.find(hval);
	if( findItor != g_HashMap.end() )
		return findItor->second;
#endif
	
	static char str[11];
	snprintf(str, 11, "0x%x", hval);
	return str;
}
