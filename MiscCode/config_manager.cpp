//
//  config_manager.cpp
//  MiscCode
//
//  Created by Morgenroth, Kyle on 11/17/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#include "config_manager.h"

const ConfigManager::Config::Value ConfigManager::Config::Value::INVALID;

ConfigManager::Config::Config()
{
}

ConfigManager::Config::~Config()
{
}

ConfigManager::Config::Value ConfigManager::Config::value(hash_t name) const
{
	const auto findItor = m_values.find(name);
	if( findItor != m_values.end() )
		return Value(&findItor->second, m_children, m_pool);
	
	return Value::INVALID;
}

const ConfigManager::Config& ConfigManager::Config::child(hash_t name) const
{
	const auto findItor = m_values.find(name);
	
	assert( findItor != m_values.end() );
	assert( findItor->second.m_type == ValueData::eCHILD );
	
	return m_children[findItor->second.m_value.o];
}

bool ConfigManager::Config::addValue(hash_t name, int32_t value)
{
	if( contains(name) )
		return false;
	
	m_values[name].set(value);
	return true;
}

bool ConfigManager::Config::addValue(hash_t name, float value)
{
	if( contains(name) )
		return false;
	
	m_values[name].set(value);
	return true;
}

bool ConfigManager::Config::addValue(hash_t name, const char* value)
{
	if( contains(name) )
		return false;
	
	m_values[name].set((uint32_t)(unsigned long)m_pool.addString(value), ValueData::eSTRING);
	return true;
}

ConfigManager::Config& ConfigManager::Config::addChild(hash_t name)
{
	const auto findItor = m_values.find(name);
	if( findItor == m_values.end() || findItor->second.m_type != ValueData::eCHILD )
	{
		m_values[name].set((uint32_t)m_children.size(), ValueData::eCHILD);
	
		m_children.resize(m_children.size() + 1);

		return m_children.back();
	}
	
	return m_children[findItor->second.m_value.o];
}

void ConfigManager::Config::hintNumValues(size_t num)
{
}

void ConfigManager::Config::hintNumChildren(size_t num)
{
	m_children.reserve(num);
}