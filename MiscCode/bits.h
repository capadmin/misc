//
//  bits.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/24/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef bits_h
#define bits_h

inline unsigned int nextPow2(unsigned int v)
{
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;
    return v;
}

// Fast Inverse Square-Root
//http://en.wikipedia.org/wiki/Fast_inverse_square_root
inline float invsqrtf(float v)
{
    union
    {
        float f;
        int i;
    } value;
    
    const float threeHalfs = 1.5f;
    const float halfV = v * 0.5f;

    value.f = v;
    value.i = 0x5f3759df - (value.i >> 1);
    value.f = value.f * (threeHalfs - (halfV * value.f * value.f));
    //value.f = value.f * (threeHalfs - (halfV * value.f * value.f));
    
    return value.f;
}

#endif