//
//  parts_manager.cpp
//  MiscCode
//
//  Created by Morgenroth, Kyle on 11/10/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#include "parts_manager.h"

#include "memory.h"

PartsManager::PartsManager()
{
}

PartsManager::~PartsManager()
{
	DELETE_SAFE_MAP(m_slots);
}

bool PartsManager::addSlot(const char* name)
{
	return addSlot(hash(name));
}

bool PartsManager::addSlot(hash_t name)
{
	auto findItor = m_slots.find(name);
	if( findItor == m_slots.end() )
		return false;
	
	m_slots[name] = nullptr;
	
	return true;
}

bool PartsManager::setSlot(PartsManager::Part* part, PartsManager::Part** oldPart)
{
	auto findItor = m_slots.find(part->slot());
	if( findItor == m_slots.end() )
		return false;
	
	auto itorEnd = m_slots.end();
	for( auto itor = m_slots.begin(); itor != itorEnd; ++itor )
		if( itor->second != nullptr && !itor->second->isCompatible(part) )
			return false;
	
	if( oldPart != nullptr )
		*oldPart = findItor->second;
	else
		DELETE_SAFE(findItor->second);
	
	findItor->second = part;
	
	return false;
}

bool PartsManager::unsetSlot(const char* name, PartsManager::Part** oldPart)
{
	return unsetSlot(hash(name), oldPart);
}

bool PartsManager::unsetSlot(hash_t name, PartsManager::Part** oldPart)
{
	auto findItor = m_slots.find(name);
	if( findItor == m_slots.end() )
		return false;
	
	if( oldPart != nullptr )
		*oldPart = findItor->second;
	else
		DELETE_SAFE(findItor->second);
	
	findItor->second = nullptr;
	
	return true;
}

const PartsManager::Part* PartsManager::getSlot(const char* name) const
{
	return getSlot(hash(name));
}

const PartsManager::Part* PartsManager::getSlot(hash_t name) const
{
	auto findItor = m_slots.find(name);
	
	return findItor == m_slots.end() ? nullptr : findItor->second;
}