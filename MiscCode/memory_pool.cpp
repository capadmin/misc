//
//  memory_pool.cpp
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/24/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

/**
	Alternate BlockPool Storage
	* Power-of-2 block size
	* One pool per block size
	- Faster indexing
		Find PoT of requested size, count bits, search upwards
*/

#include "memory_pool.h"

#include <cstring>

#include "memory.h"

#include "log.h"

#include "bits.h"

/****
** Memory::BlockPool
****/

Memory::BlockPool::BlockPool(unsigned int size, unsigned int num, bool clearOnFree)
: m_clearOnFree(clearOnFree)
, m_blockSize(size)
, m_numBlocks(num < 32 ? 32 : nextPow2(num))
, m_numStates(m_numBlocks >> 5)
, m_states(nullptr)
, m_memory(nullptr)
{
	const unsigned int numStates = m_numStates + 1;

    m_states = new unsigned int[numStates];
    memset(m_states, 0, sizeof(unsigned int) * numStates);
    
    m_memory = new unsigned char[m_numBlocks * m_blockSize];
}

Memory::BlockPool::~BlockPool()
{
    DELETE_SAFE_ARR(m_states);
    DELETE_SAFE_ARR(m_memory);
}

unsigned int Memory::BlockPool::blockSize()
{
	return m_blockSize;
}

unsigned int Memory::BlockPool::numBlocks()
{
    return m_numBlocks;
}

unsigned int Memory::BlockPool::freeBlocks()
{
    unsigned int freeBlocks = 0;
    
    for( unsigned int i = 0; i < m_numStates; ++i )
    {
        const unsigned int state = m_states[i];
        for( unsigned int j = 0; j < 32; ++j )
            if( (state & 1 << j) == 0 )
                ++freeBlocks;
    }

    return freeBlocks;
}

void* Memory::BlockPool::alloc()
{
    // There are always an empty state at the end
	unsigned int* states = m_states;
    while( *states == 0xFFFFFFFF )
		++states;
    
	const unsigned int stateIndex = (unsigned int)(states - m_states);
    if( stateIndex >= m_numStates )
		return nullptr;

    // We know there is an unset bit somewhere
	const unsigned int state = *states;
    unsigned int bitIndex = 0;
    while((state & (1 << bitIndex)) > 0)
        ++bitIndex;
    
    *states |= (1 << bitIndex);
    
    const unsigned int blockIndex = ((stateIndex << 5) | bitIndex);
    
    return m_memory + (blockIndex * m_blockSize);
}

bool Memory::BlockPool::free(void* ptr)
{
	const unsigned int blockIndex = (unsigned int)((unsigned char*)ptr - m_memory) / m_blockSize;
	if( blockIndex >= m_numBlocks )
		return false;

	const unsigned int stateIndex = blockIndex >> 5;
	const unsigned int bitIndex = blockIndex & 31;

#if defined(DEBUG)
	if( (m_states[stateIndex] & (1 << bitIndex)) == 0 )
		ERRORF("Block [%d] is already free!", blockIndex);

	if( m_clearOnFree )
		memset(ptr, 0xFE, m_blockSize);
#endif

	m_states[stateIndex] &= ~(1 << bitIndex);

	return true;
}

/****
** Memory::DynamicPool
****/

/**
	|Headers->					<-Fragments|
	|H|H|H|-------------------|~~~|~|~~~~~~|
	m_memory						(m_memory + size)
*/

Memory::DynamicPool::DynamicPool(unsigned int size, bool defragOnFree, bool clearOnFree)
: m_clearOnFree(clearOnFree)
, m_defragOnFree(defragOnFree)
, m_size(size)
, m_memory(new unsigned char[m_size])
{
	m_last = (Header*)m_memory;
	m_last->used = false;
	m_last->size = 0;
	m_last->last = true;
	m_last->offset = size;

	m_numFragments = 1;
}

Memory::DynamicPool::~DynamicPool()
{
	DELETE_SAFE_ARR(m_memory);
}

void* Memory::DynamicPool::alloc(unsigned int size)
{
    const unsigned int lastSize = m_last->offset - (m_numFragments * sizeof(Header));
    
    // Scan for unused fragments that will fit, ignoring the last fragment
	Header* fragment;
	unsigned int fragIndex = 0;
	do
	{
		fragment = (Header*)(m_memory + (sizeof(Header) * fragIndex));
		if( !fragment->used && !fragment->last && fragment->size >= size )
			break;
		++fragIndex;
	}
	while( fragIndex < m_numFragments );

    // If there were no available fragments
	if( fragIndex >= m_numFragments )
    {
        // Check the last and see if it's available
        if( m_last->used || lastSize < size )
            return nullptr;

        fragment = m_last;
    }

	fragment->used = true;

    // If there's enough space for another Header
    if( lastSize - size > sizeof(Header) )
    {
        Header* newFragment = (Header*)(m_memory + (sizeof(Header) * m_numFragments));

        newFragment->used = false;
        newFragment->last = fragment->last;
        
        // If the current fragment is last, the new fragment
        // we be last
        if( fragment->last )
        {
            fragment->size = size;
            fragment->offset -= size;
            fragment->last = false;
            
            newFragment->size = 0;
            newFragment->offset = fragment->offset;
            m_last = newFragment;
        }
        // If it's not last, simply divide
        else
        {
            newFragment->size = fragment->size - size;
            newFragment->offset = fragment->offset;
            
            fragment->offset += newFragment->size;
            fragment->size -= newFragment->size;
        }
        
        ++m_numFragments;
    }
    // else if there is not enough space for another Header
    else
    {
        // If last, make it not last
        // All other fragments are just marked as used
        if( fragment->last )
        {
            fragment->last = false;
            fragment->size = size;
            fragment->offset -= size;
        }
    }

    // Return address
	return (void*)(m_memory + fragment->offset);
}

bool Memory::DynamicPool::free(void* ptr)
{
    const unsigned int offset = (unsigned int)((unsigned char*)ptr - m_memory);
    if( offset >= m_size )
        return false;
    
    Header* fragment;
	unsigned int fragIndex = 0;
	do
	{
		fragment = (Header*)(m_memory + (sizeof(Header) * fragIndex));
		if( fragment->offset == offset )
			break;
		++fragIndex;
	}
	while( fragIndex < m_numFragments );
    
    if( fragIndex >= m_numFragments )
        return false;
    
#if defined(DEBUG)
    if( !fragment->used )
    {
        ERRORF("Fragment [%d] already freed!", fragIndex);
        return false;
    }
#endif
    
    fragment->used = false;
    
    // If the fragment was last, make it last again
    if( m_last == fragment )
    {
        fragment->last = true;
        fragment->offset += fragment->size;
        fragment->size = 0;
    }
    
    if( m_defragOnFree )
        defragmentStep();
    
	return true;
}

void Memory::DynamicPool::defragment(int steps)
{
    if( steps == 0 )
        return;
    
    while( defragmentStep() && --steps != 0 ) ;
}

bool Memory::DynamicPool::defragmentStep()
{
    bool defragged = false;
    
    Header* fragment;
    Header* nextFragment;
    
	unsigned int fragIndex = 0;
    unsigned int nextIndex;
    
    Header* a;
    Header* b;
    
	do
	{
		fragment = (Header*)(m_memory + (sizeof(Header) * fragIndex));
        if( fragment->used )
            continue;
        
        nextIndex = fragIndex + 1;
        
        do
        {
            nextFragment = (Header*)(m_memory + (sizeof(Header) * nextIndex));
            if( nextFragment->used )
                continue;
            
            bool merged = false;
            
            if( fragment->offset > nextFragment->offset )
            {
                a = fragment;
                b = nextFragment;
            }
            else
            {
                a = nextFragment;
                b = fragment;
            }
            
            if( a->offset == b->offset )
            {
                if( a->last )
                {
                    Header* s = a;
                    a = b;
                    b = s;
                }
                
                a->last = true;
                a->offset += a->size;
                a->size = 0;
                m_last = a;
                merged = true;
            }
            else if( a->offset == b->offset + b->size )
            {
                a->offset -= b->size;
                a->size += b->size;
                merged = true;
            }
            
            if( !merged )
                ++nextIndex;
            else
            {
                Header* lastFragment = (Header*)(m_memory + (sizeof(Header) * (m_numFragments - 1)));
                *b = *lastFragment;
                --m_numFragments;
                defragged = true;
            }
        }
        while( nextIndex < m_numFragments );

		++fragIndex;
	}
	while( fragIndex < m_numFragments - 1 );
    
    return defragged;
}

/****
** Memory
****/

Memory::Memory()
: m_blockPools(nullptr)
, m_numBlockPools(0)
, m_dynamicPools(nullptr)
, m_numDynamicPools(0)
, m_bigBlocks(nullptr)
, m_numBigBlocks(0)
{
}

Memory::~Memory()
{
	for( unsigned int i = 0; i < m_numBlockPools; ++i )
		if( m_blockPools[i].owned )
			DELETE_SAFE(m_blockPools[i].pool);

	DELETE_SAFE(m_blockPools);
    
    for( unsigned int i = 0; i < m_numDynamicPools; ++i )
		if( m_dynamicPools[i].owned )
			DELETE_SAFE(m_dynamicPools[i].pool);
    
	DELETE_SAFE(m_dynamicPools);
    
    for( unsigned int i = 0; i < m_numBigBlocks; ++i )
        DELETE_SAFE(m_bigBlocks[i].memory);
    
	DELETE_SAFE(m_bigBlocks);
}

void Memory::addPool(Memory::BlockPool* pool, bool own)
{
	const unsigned int blockSize = pool->blockSize();
	unsigned int index;
	for( index = 0; index < m_numBlockPools; ++index )
		if( m_blockPools[index].blockSize > blockSize )
			break;
	
	BlockPoolContainer* pools = new BlockPoolContainer[m_numBlockPools + 1];

	if( index == 0 )
		memcpy(pools + 1, m_blockPools, sizeof(BlockPoolContainer) * m_numBlockPools);
	else if( index == m_numBlockPools )
		memcpy(pools, m_blockPools, sizeof(BlockPoolContainer) * m_numBlockPools);
	else
	{
		memcpy(pools, m_blockPools, sizeof(BlockPoolContainer) * index);
		memcpy(pools + index + 1, m_blockPools + index, sizeof(BlockPoolContainer) + (m_numBlockPools - index));
	}

	BlockPoolContainer& container = pools[index];
	container.blockSize = blockSize;
	container.owned = own;
	container.pool = pool;

	DELETE_SAFE_ARR(m_blockPools);
	
	m_blockPools = pools;
	++m_numBlockPools;
}

void Memory::addPool(Memory::DynamicPool* pool, bool own)
{
	DynamicPoolContainer* pools = new DynamicPoolContainer[m_numDynamicPools + 1];
    
    memcpy(pools, m_dynamicPools, sizeof(DynamicPoolContainer) * m_numDynamicPools);
	
	DynamicPoolContainer& container = pools[m_numDynamicPools];
	container.owned = own;
	container.pool = pool;
    
	DELETE_SAFE_ARR(m_dynamicPools);
	
	m_dynamicPools = pools;
	++m_numDynamicPools;
}

void* Memory::alloc(unsigned int size)
{
    if( m_numBlockPools > 0 )
    {
        const BlockPoolContainer* container = m_blockPools;
        const BlockPoolContainer* containerEnd = m_blockPools + m_numBlockPools;

        do
        {
            if( container->blockSize >= size )
            {
                void* ptr = container->pool->alloc();
                if( ptr != nullptr )
                    return ptr;
            }

            ++container;
        }
        while( container != containerEnd );
    }
    
    if( m_numDynamicPools > 0 )
    {
        const DynamicPoolContainer* container = m_dynamicPools;
        const DynamicPoolContainer* containerEnd = m_dynamicPools + m_numDynamicPools;
        
        do
        {
            void* ptr = container->pool->alloc(size);
            if( ptr != nullptr )
                return ptr;
            
            ++container;
        }
        while( container != containerEnd );
    }

	return nullptr;
}

void Memory::free(void* ptr)
{
    if( m_numBlockPools > 0 )
    {
        const BlockPoolContainer* container = m_blockPools;
        const BlockPoolContainer* containerEnd = m_blockPools + m_numBlockPools;

        do
        {
            if( container->pool->free(ptr) )
                return;

            ++container;
        }
        while( container != containerEnd );
    }
    
    if( m_numDynamicPools > 0 )
    {
        const DynamicPoolContainer* container = m_dynamicPools;
        const DynamicPoolContainer* containerEnd = m_dynamicPools + m_numDynamicPools;
        
        do
        {
            if( container->pool->free(ptr) )
                return;
            
            ++container;
        }
        while( container != containerEnd );
    }
    
    WARNF("Could not free memory [0x%x]!", ptr);
}

bool Memory::createBigBlock(unsigned int id, unsigned int size)
{
    if( id < m_numBigBlocks && m_bigBlocks[id].memory != nullptr )
    {
        ERRORF("BigBlock [%d] already allocated!", id);
        return false;
    }
    
    if( id >= m_numBigBlocks )
    {
        BigBlockContainer* newBigBlocks = new BigBlockContainer[id + 1];
        memset(newBigBlocks, 0, sizeof(BigBlockContainer) * (id + 1));
        memcpy(newBigBlocks, m_bigBlocks, sizeof(BigBlockContainer) * m_numBigBlocks);
        
        DELETE_SAFE_ARR(m_bigBlocks);
        
        m_bigBlocks = newBigBlocks;
        m_numBigBlocks = id + 1;
    }
    
    BigBlockContainer& container = m_bigBlocks[id];
    container.size = size;
    container.allocated = false;
    container.memory = new unsigned char[size];
    
    return true;
}

void* Memory::allocBigBlock(unsigned int id)
{
    if( id >= m_numBigBlocks )
        return nullptr;
    
    BigBlockContainer& container = m_bigBlocks[id];
    
#if defined(DEBUG)
    if( container.memory == nullptr )
        WARNF("BigBlock [%d] not allocated!", id);
#endif
    
    if( container.allocated )
    {
        WARNF("BigBlock [%d] already allocated!", id);
        return nullptr;
    }
    
    container.allocated = true;
    
    return container.memory;
}

void Memory::freeBigBlock(unsigned int id)
{
    if( id >= m_numBigBlocks )
        return;
    
    BigBlockContainer& container = m_bigBlocks[id];
    
#if defined(DEBUG)
    if( container.memory == nullptr )
        WARNF("BigBlock [%d] not allocated!", id);

    if( container.allocated )
        WARNF("BigBlock [%d] already allocated!", id);
#endif
    
    container.allocated = false;
}



