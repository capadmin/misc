//
//  string_pool.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 11/16/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef string_pool_h
#define string_pool_h

#include <cstdint>

/**
 * String Pool base implementation
 * Handles memory management, and basic string addition and retrieval
 * Should not be used directly
 */
class StringPoolBase
{
public:
	/**
	 * Destructor
	 */
	~StringPoolBase();
	
protected:
	/**
	 * Constructor
	 * @param	size	Size of the pool, if !0 the pool is statically sized
	 */
	StringPoolBase(unsigned int size);
	
	/**
	 * Add a string to the pool
	 * @param str		String data
	 * @param length	Length of string data, -1 will use strlen() to determine the length
	 * @return			Address of the strings location in the pool, or nullptr if it failed
	 */
	const char* addString(const char* str, int length = -1);
	
	/**
	 * Get the location of a string in the pool
	 * @param str		String data
	 * @param length	Length of string data, -1 will use strlen() to determine the length
	 * @return			Address of the strings location in the pool, or nullptr if it failed or was not found
	 */
	const char* getString(const char* offset, int length = -1) const;
	
	/**
	 * Compact memory to smallest size necessary to store the data
	 */
	void compact();
	
	/**
	 * Get pool data
	 */
	const char* pool() const { return m_pool; }
	
	/**
	 * Get pool size
	 */
	uint32_t size() const { return m_size; }
	
	/**
	 * Get pool length
	 */
	uint32_t length() const { return m_length; }
	
private:
	// String data pool
	char* m_pool;
	
	// Size of the pool, in bytes
	unsigned int m_size;
	
	// Length of the used pool, in bytes
	unsigned int m_length;
	
	// Is statically sized
	bool m_static;
};

/**
 * Dynamic-sized String Pool
 * Allocates the pool space as needed
 * and references strings by id/offset
 */
class DynamicStringPool : public StringPoolBase
{
public:
	/**
	 * Constructor
	 */
	DynamicStringPool()
	: StringPoolBase(-1)
	{
	}
	
	/**
	 * Add a string to the pool
	 * @param str		String data
	 * @param length	Length of string data, -1 will use strlen() to determine the length
	 * @return			Offset of the string, or -1 if it failed
	 */
	uint32_t addString(const char* str, int length = -1)
	{
		str = StringPoolBase::addString(str, length);
		if( str == nullptr )
			return -1;
		
		return (uint32_t)(str - pool());
	}
	
	/**
	 * Get the string stored in the pool
	 * @param offset	String offset
	 * @return			Address of the string, or nullptr if it failed or was not found
	 */
	const char* getString(uint32_t offset) const
	{
		if( offset >= length() )
			return nullptr;
		
		return pool() + offset;
	}
	
	/**
	 * Compact memory to smallest size necessary to store the data
	 */
	void compact()
	{
		StringPoolBase::compact();
	}
};

/**
 * Statically-sized string pool
 * Strings are referenced by pointer, but it can
 * run out of space
 */
class StaticStringPool : public StringPoolBase
{
public:
	/**
	 * Constructor
	 * @param size	Size of the pool
	 */
	StaticStringPool(unsigned int size)
	: StringPoolBase(size)
	{
	}
	
	/**
	 * Add a string to the pool
	 * @param str		String data
	 * @param length	Length of string data, -1 will use strlen() to determine the length
	 * @return			Address of the strings location in the pool, or nullptr if it failed
	 */
	const char* addString(const char* str, int length = -1)
	{
		return StringPoolBase::addString(str, length);
	}
	
	/**
	 * Get the location of a string in the pool
	 * @param str		String data
	 * @param length	Length of string data, -1 will use strlen() to determine the length
	 * @return			Address of the strings location in the pool, or nullptr if it failed or was not found
	 */
	const char* getString(const char* str, int length = -1) const
	{
		return StringPoolBase::getString(str, length);
	}
};

#endif
