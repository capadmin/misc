//
//  entity.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/28/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef entity_h
#define entity_h

#include "vector2.h"

class Entity
{
public:
	/**
	 * Constructor
	 * @param maxVelocity	Maximum straight-line velocity
	 * @param maxAccel		Maximum acceleration
	 * @param maxBrake		Maximum stopping acceleration
	 * @param maxTurn		Maximum turning angle
	 * @param maxTurnVel	Maximum velocity allowed when turned to maximum turn angle
	 */
    Entity(float maxVelocity = 1.0f, float maxAccel = 1.0f, float maxBrake = 1.0f, float maxTurn = 15.0f, float maxTurnVel = 1.0f);
	
	/**
	 * Deconstructor
	 */
    ~Entity() {}
    
	/**
	 * Get maximum velocity
	 */
	float maxVelocity() const { return m_maxVelocity; }
	
	/**
	 * Get maximum acceleration
	 */
	float maxAcceleration() const { return m_maxAcceleration; }
	
	/**
	 * Get maximum stopping acceleration
	 */
	float maxBrake() const { return m_maxBrake; }
	
	/**
	 * Get maximum velocity when turned to maximum turn angle
	 */
	float maxTurnVelocity() const { return m_maxTurnVelocity; }
	
	/**
	 * Get maximum turn angle
	 */
	float maxTurnAngle() const{ return m_maxTurnAngle; }

	/**
	 * Get position
	 */
	const vector2& position() const { return m_position; }
	
	/**
	 * Get direction
	 */
	const vector2& direction() const { return m_direction; }
	
	/**
	 * Get velocity
	 */
	float velocity() const { return m_velocity; }
	
	/**
	 * Get acceleration
	 */
	float acceleration() const { return m_acceleration; }
	
	/**
	 * Get turn angle
	 */
	float turnAngle() const { return m_turnVelocity; }

	/**
	 * Get destination
	 */
	const vector2& destination() const { return m_destination; }
	
	/**
	 * Get destination range
	 */
	float destinationRange() const { return m_destinationRange; }

	/**
	 * Set destination
	 * @param destination	Destinaton location
	 * @param range			Distance from destination location that is acceptable
	 * @param				Return true if the destination is reachable and set, false otherwise
	 */
	bool setDestination(const vector2& destination, float range);

	/**
	 * Update
	 * @param	t		Time, in seconds
	 * @return			True if the destination has been reached, false otherwise
	 */
    bool update(float t);
    
	/**
	 * Debug logging function
	 */
	void log();

private:
	// Maximum velocity
    const float m_maxVelocity;
	
	// Maximum acceleration
    const float m_maxAcceleration;
	
	// Maximum stopping acceleration
    const float m_maxBrake;
	
	// Maximum turning angle
    const float m_maxTurnAngle;
	
	// Maximum velocity when at maximum turn angle
    const float m_maxTurnVelocity;
    
	// Current position
    vector2 m_position;
	
	// Current direction, normalized
    vector2 m_direction;
	
	// Current velocity
    float m_velocity;
	
	// Current acceleration
    float m_acceleration;
	
	// Current turn angle velocity
    float m_turnVelocity;
    
	// Current destination
    vector2 m_destination;
	
	// Current destination range - acceptable distance from destination
    float m_destinationRange;
};

#endif
