//
//  interpolate.h
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/9/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#ifndef interpolate_h
#define interpolate_h

namespace Interpolate
{
    float linear(float speed, float value, float max, float delta);
    
    float linear(float start, float end, float t);
    
    float sine(float value, float end, float delta);
    
    float power(float value, float end, float delta);
}

#endif /* defined(__MiscCode__interpolate__) */
