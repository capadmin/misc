//
//  entity.cpp
//  MiscCode
//
//  Created by Morgenroth, Kyle on 10/28/13.
//  Copyright (c) 2013 Morgenroth, Kyle. All rights reserved.
//

#include "entity.h"

#include "log.h"

#include "numbers.h"

Entity::Entity(float maxVel, float maxAccel, float maxBrake, float maxTurn, float maxTurnVel)
: m_maxVelocity(maxVel)
, m_maxAcceleration(maxAccel)
, m_maxBrake(maxBrake)
, m_maxTurnAngle(DEG2RAD(maxTurn))
, m_maxTurnVelocity(maxTurnVel)
, m_position(0, 0)
, m_direction(0, 1)
, m_velocity(0)
, m_acceleration(0)
, m_turnVelocity(0)
, m_destination(0, 0)
, m_destinationRange(0)
{
}

bool Entity::setDestination(const vector2& destination, float range)
{
	// Given the maximum turn angle and velocity, the circular areas that cannot be reached directly
	// can be calculated and used for comparison
	const float turnRadius = m_maxTurnVelocity / tanf(m_maxTurnAngle);
	const float dist = turnRadius * turnRadius; //(turnRadius - range) * (turnRadius - range);

	// "Left" side check
	const vector2 a = vector2(-m_direction.y, m_direction.x) * turnRadius + m_position;
	const float aDist = (destination - a).lengthSqr();

	// "Right" side check
	const vector2 b = vector2(m_direction.y, -m_direction.x) * turnRadius + m_position;
	const float bDist = (destination - b).lengthSqr();
	
	const bool canReach = ( bDist > dist && aDist > dist );

	if( canReach )
	{
		m_destination = destination;
		m_destinationRange = range;
	}
	else
	{
		m_destination.set(0, 0);
		m_destinationRange = 0;
	}

	return canReach;
}

bool Entity::update(float t)
{
	bool hasArrived = false;
	
	// No destination
	if( m_destinationRange <= 0 )
	{
		if( isZero(m_velocity) )
		{
			m_velocity = 0.0f;
			m_acceleration = 0.0f;
			return true;
		}
		
		// Come to a stop
		m_acceleration = -m_maxBrake;
		hasArrived = false;
	}
	else
	{
		vector2 dirToDestination = m_destination - m_position;
		const float distToDestination = dirToDestination.length();
		
		// If the destination has been reached and the entity is stopped,
		// reset all of the values
		if( distToDestination <= m_destinationRange && isZero(m_velocity) )
		{
			m_destination.set(0.0f, 0.0f);
			m_destinationRange = 0.0f;

			m_acceleration = 0.0f;
			m_velocity = 0.0f;

			hasArrived = true;
		}
		else
		{
			dirToDestination /= distToDestination;

			//// Direction
			
			// Find the angle to destination, and clamp it
			float angleBetween = m_direction.angleBetween(dirToDestination);
			if( fabs(angleBetween) < DEG2RAD(0.25f) )
				angleBetween = 0.0f;

			const float turnAcceleration = 10.0f * m_maxTurnAngle;
			
			// Angular distance between the current direction and the desired direction
			const float turnDistance = angleBetween - m_turnVelocity;

			// Angular distance required to smoothly decelerate to a stop
			const float turnStopDistance = (m_turnVelocity * m_turnVelocity) / (2.0f * turnAcceleration);

			// If the entity is facing the desired direction (turnDistance == 0) or
			// the remaining turn distance <= to the stop distance, decelerate
			float turnVelocityDelta =
				(isZero(turnDistance) || turnStopDistance <= fabs(turnDistance) ? 1 : -1) * sign(turnDistance) * turnAcceleration * t;
			
			// If the delta would cause it to over-turn, clamp it
			if( fabs(turnVelocityDelta) > fabs(turnDistance) )
				turnVelocityDelta = turnDistance;
			
			m_turnVelocity += turnVelocityDelta;
			
			// If the turn exceeds to the maximum, clamp it
			if( fabs(m_turnVelocity) > m_maxTurnAngle )
				m_turnVelocity = sign(m_turnVelocity) * m_maxTurnAngle;

			//// Velocity
			
			// Distance required to smoothly decelerate to a stop
			const float stopDistance = (m_velocity * m_velocity) / (2 * m_maxBrake);
			
			// Aim for zero velocity inside the range, but perhaps not precisely at the location
			if( distToDestination - ((4.0f / 6.0f) * m_destinationRange) <= stopDistance || distToDestination <= m_destinationRange )
				m_acceleration = -m_maxBrake;
			else
				m_acceleration = m_maxAcceleration;
		}
	}

	// Maximum velocity allowed, provided the current turn angle
	const float maxVelocity =
		((!isZero(m_turnVelocity) ? (1.0f - (fabs(m_turnVelocity) / m_maxTurnAngle)) : 1.0f) *
		(m_maxVelocity - m_maxTurnVelocity)) + m_maxTurnVelocity;
	
	// Apply acceleration and clamp to maximum velocity
	m_velocity += (m_acceleration * t);

	m_velocity = CLAMP(m_velocity, 0, maxVelocity);

	// Calculate distance traveled
	const float distTraveled = m_velocity * t;

	// Can only turn if moving
	if( !isZero(distTraveled) && !isZero(m_turnVelocity) )
		m_direction.rotate(m_turnVelocity * t);

	// Advance
	m_position += m_direction * distTraveled;

	return hasArrived;
}

void Entity::log()
{
	/*
	 LOGF("Position: <%.03f, %.03f>", m_position.x, m_position.y);
	 LOGF("Direction: <%.03f, %.03f>", m_direction.x, m_direction.y);
	 LOGF("DirToDest: <%.03f, %.03f>", dirToDestination.x, dirToDestination.y);
	 LOGF("Angle Between: <%.03f>", RAD2DEG(angleBetween));
	 LOGF("Turn Distance: <%.03f>", RAD2DEG(turnDistance));
	 LOGF("Stop Distance: <%.03f>", RAD2DEG(turnStopDistance));
	 LOGF("Turn Delta: <%.03f>", RAD2DEG(turnVelocityDelta));
	 LOGF("Turn: <%.03f>", RAD2DEG(m_turnVelocity));
	 

	 LOGF("Direction: <%.03f, %.03f>", m_direction.x, m_direction.y);
	 LOGF("DirToDest: <%.03f, %.03f>", dirToDestination.x, dirToDestination.y);
	 LOGF("Angle Between: <%.03f>", RAD2DEG(angleBetween));
	 LOGF("Stop Distance: <%.03f>", turnStopDistance);
	 LOGF("Turn Delta: <%.03f>", RAD2DEG(turnVelocityDelta));
	 	LOGF("Max Velocity: <%0.3f>", maxVelocity);
	 */
	//LOGF("Distance: <%.03f>",  (m_destination - m_position).length());
	//LOGF("Position: <%.03f, %.03f>", m_position.x, m_position.y);
	//LOGF("Direction: <%.03f, %.03f>", m_direction.x, m_direction.y);
	//LOGF("Turn Angle: <%.03f>", RAD2DEG(m_turnVelocity));
	//LOGF("Velocity: <%.03f>", m_velocity);
	//LOGF("Acceleration: <%.03f>", m_acceleration);
}